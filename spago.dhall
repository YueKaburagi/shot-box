{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name =
    "my-project"
, dependencies =
    [ "console"
    , "effect"
    , "errors"
    , "js-timers"
    , "maybe"
    , "nonempty"
    , "parsing"
    , "promises"
    , "psci-support"
    , "quickcheck"
    , "spec"
    , "spec-quickcheck"
    , "web-html"
    , "web-uievents"
    ]
, packages =
    ./packages.dhall
, sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
}
