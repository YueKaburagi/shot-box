module Test.Main where

import Prelude

import Effect (Effect)
import Effect.Aff (launchAff_, Aff)
import Effect.Class (liftEffect)
import Test.Spec (pending, describe, it, SpecT)
import Test.Spec.Assertions (shouldSatisfy, shouldReturn)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (runSpecT, defaultConfig)
import Test.Spec.QuickCheck (quickCheck)
import Test.QuickCheck ((<?>), Result)

import Data.Either (Either(..), isLeft)
import Data.Maybe (Maybe(..))
import Text.Parsing.Parser (runParserT)

import Shotbox.Shotbox.Parser (parserNav, NavControl(..),
                               StrGenAtom(..), Constraint(..), parserStrGen)
import Message (BgMessage, encode, decode)


main :: Effect Unit
main = do
  aff <- runSpecT defaultConfig [consoleReporter] spec
  launchAff_ aff


spec :: SpecT Aff Unit Effect Unit
spec =
  describe "shotbox" do
    describe "parse Nav" do
      describe "success" do
        it "atomA" do
          parseNav "single" `shouldReturn` Right (Search false ["single"])
        it "atomB" do
          parseNav "\"comm ant\"" `shouldReturn` Right (Search false ["comm ant"])
        it "end by whitespace" do
          parseNav "after-white     " `shouldReturn` Right (Search false ["after-white"])
        it "begin by whitespace" do
          parseNav "    before-white" `shouldReturn` Right (Search false ["before-white"])
        it "between whitespace" do
          parseNav "    with-white     " `shouldReturn` Right (Search false ["with-white"])
        it "open outer" do
          parseNav "+ willow" `shouldReturn` Right (Search true ["willow"])
        it "multi args" do
          parseNav "abc def gzd"`shouldReturn` Right (Search false ["abc","def","gzd"])
        it "atomB after" do
          parseNav "\"atomB\" after atomA" `shouldReturn` Right (Search false ["atomB","after","atomA"])
        it "multi whitespaces" do
          parseNav "+   hoge 　 \t\r\n \"piyo piyo\"　 \t\r\n fuga" `shouldReturn` Right (Search true ["hoge","piyo piyo","fuga"])
        it "abc \"\"" do
          parseNav "abc \"\"" `shouldReturn` Right (Search false ["abc",""])
        it "abc \"def\"ghi" do
          parseNav "abc \"def\"ghi" `shouldReturn` Right (Search false ["abc","def","ghi"])
      describe"failure" do
        it "+ only" do 
          parseNav "+" `shouldReturnSatisfy` isLeft

    describe "parse StrGen" do
      describe "success" do
        it "straight" do
          parseStrGen "a text" `shouldReturn` Right [Text "a text"]
        it "pure url" do
          parseStrGen "<url>" `shouldReturn` Right [Url Through]
        it "pure title" do
          parseStrGen "<title>" `shouldReturn` Right [Title Through]
        it "with constraint" do
          parseStrGen "<url:replacer>" `shouldReturn` Right [Url $ ReStrCold "replacer"]
        it "valid regex" do
          parseStrGen "<title:(.+) - nowe>" `shouldReturn` Right [Title $ ReStrCold "(.+) - nowe"]
        it "more :" do
          parseStrGen "<url::::>" `shouldReturn` Right [Url $ ReStrCold ":::"]
        it "between" do
          parseStrGen "abc<url>def" `shouldReturn` Right [Text "abc", Url Through, Text "def"]
      describe "failure" do
        it "unexpected >" do
          parseStrGen "a text > on" `shouldReturnSatisfy` isLeft
        it "unclosed <" do
          parseStrGen "a text < unclosed" `shouldReturnSatisfy` isLeft

    describe "encode/decode Message" do
      it "identity" do
        quickCheck ecdecCheck

parseNav s = liftEffect $ runParserT s (parserNav false)
parseStrGen s = liftEffect $ runParserT s (parserStrGen '<' '>')




shouldReturnSatisfy m f = (_ `shouldSatisfy` f) =<< m




ecdecCheck :: BgMessage -> Result
ecdecCheck b = (Just b == r) <?> ("Fail identity check. \n\tExpected ''" <> show b <> "''\n\tActually ''" <> show r <> "''\n\t  Intermediate state ''" <> show d <> "''")
  where
    d = encode b
    r = decode d
