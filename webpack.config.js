const webpack = require('webpack');
const path = require('path');

module.exports = {
  mode: 'development',
  entry: "./src/simple.js",
  output: {
    filename: "simple.js",
    path: path.join(__dirname, 'ut')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader'
      }
    ]
  }
};
