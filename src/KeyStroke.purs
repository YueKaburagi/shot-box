
module KeyStroke where

import Prelude

import Data.Foldable (intercalate)

import Web.UIEvent.KeyboardEvent as Kbd
import Web.UIEvent.KeyboardEvent (KeyboardEvent)

import Action.Types (SboxFunction)


type Key = { ctrl :: Boolean, alt :: Boolean, meta :: Boolean, key :: String }
type Stroke = Array Key
type ActRecord = { stroke :: Stroke, box :: SboxFunction }
type KeyMap = { level :: String, records :: Array ActRecord }
initKey :: String -> Key
initKey k = { ctrl : false, alt : false, meta : false, key : k }
withCtrl :: Key -> Key
withCtrl = _ { ctrl = true }
withAlt :: Key -> Key
withAlt = _ { alt = true }
withMeta :: Key -> Key
withMeta = _ { meta = true }

evnFromKey :: Boolean -> KeyboardEvent -> Key
evnFromKey false k =
  { ctrl : Kbd.ctrlKey k
  , alt : Kbd.altKey k
  , meta : Kbd.metaKey k
  , key : Kbd.key k
  }
evnFromKey true k =
  { ctrl : Kbd.ctrlKey k
  , alt : false
  , meta : (Kbd.altKey k || Kbd.metaKey k)
  , key : Kbd.key k
  }

strokeToString :: Stroke -> String
strokeToString = intercalate " " <<< map keyToString
keyToString :: Key -> String
keyToString k =
  (if k.alt then "A-" else "")
  <> (if k.ctrl then "C-" else "")
  <> (if k.meta then "M-" else "")
  <> k.key


-- exists instance of Eq for Record
--keyEq :: Key -> Key -> Boolean
--keyEq k l =
--  k.alt == l.alt && k.ctrl == l.ctrl && k.meta == l.meta && k.key == l.key
