module Main where

import Prelude

import Effect (Effect)
import Effect.Console as Console
import Effect.Timer as Timer
import Data.Maybe (Maybe(..), maybe)
import Data.Array as Array
import Data.Tuple (Tuple(..))
import Control.Comonad (extract)

import Web.HTML as HTML
import Web.HTML.Window (Window)
import Web.HTML.Window as Window
import Web.HTML.HTMLElement as HTMLElement

import Web.Event.EventTarget (eventListener, addEventListener)
import Web.Event.Event (EventType(..), Event)
import Web.Event.Event as Event
import Web.UIEvent.KeyboardEvent as Kbd

import Action.Text as AT
import Action.Page as AP
import Action.Tab as AX
import Action.Shotbox as AS
import Action.Types (SboxFunction)
import State (getGlobal, setGlobal, modifyGlobal, initState)
import Configure (getConfig, boil)
import KeyStroke (ActRecord, Key, evnFromKey, initKey, strokeToString, withCtrl, withMeta)
import Page.Connection (makeConnection)
import Shotbox.Shotbox (addShotbox, addTextpane)
import Shotbox.Indicator (addIndicator, hideIndicator)
import Data.Cumulative (cumulated)
import Log as Log
import Util.Timer (killTimer)

main :: Effect Unit
main = do
  setGlobal initState
  modifyGlobal (_ { records = keymap })
  w <- HTML.window
  l <- eventListener (driveEntry w)
  makeConnection
  loadStrGenMap
  addEventListener (EventType "keydown") l false (Window.toEventTarget w)
  doc <- Window.document w
  addShotbox doc
  addIndicator doc
  addTextpane doc


loadStrGenMap :: Effect Unit
loadStrGenMap = do
  cold <- _.strGenMap <$> getConfig
  boiled <- boil cold
  Log.withWarnShow "load StrGenMap" (cumulated boiled)
  modifyGlobal (_ { strGenMap = extract boiled })


allRecords :: Array SboxFunction
allRecords = []

-- TODO: control keymap priorities
keymap :: Array ActRecord
keymap =
  [ { stroke : [withCtrl $ initKey "g"], box : AT.actionKeyboardQuit }
  , { stroke : [withCtrl $ initKey "f"], box : AT.emulateForwardChar }
  , { stroke : [withCtrl $ initKey "F"], box : AT.emulateForwardCharSel }
  , { stroke : [withCtrl $ initKey "b"], box : AT.emulateBackwardChar }
  , { stroke : [withCtrl $ initKey "B"], box : AT.emulateBackwardCharSel }
  , { stroke : [withCtrl $ initKey "a"], box : AT.emulateMoveBeginningOfLine }
  , { stroke : [withCtrl $ initKey "e"], box : AT.emulateMoveEndOfLine }
  , { stroke : [withCtrl $ initKey "d"], box : AT.emulateDeleteForwardChar }
  , { stroke : [withCtrl $ initKey "h"], box : AT.emulateDeleteBackwardChar }
  , { stroke : [withCtrl $ initKey "m"], box : AT.actionInsertNewline }
  , { stroke : [withCtrl $ initKey "k"], box : AT.emulateKillLine }
  , { stroke : [withMeta $ initKey "w"], box : AT.emulateKillRingSave }
  , { stroke : [withCtrl $ initKey "v"], box : AP.actionNextPage }
  , { stroke : [withCtrl $ initKey "z"], box : AP.actionPrevPage }
  , { stroke : [withCtrl $ initKey "n"], box : AP.actionNextLine }
  , { stroke : [withCtrl $ initKey "p"], box : AP.actionPrevLine }
  , { stroke : [withCtrl $ initKey "x", initKey "u", initKey "r", initKey "l"], box : AX.actionCopyUrl }
  , { stroke : [withCtrl $ initKey "x", initKey "u", initKey "t", initKey "i"], box : AX.actionCopyTitle }
  , { stroke : [withCtrl $ initKey "x", initKey "u", initKey "x", initKey "r"], box : AX.actionStrGen }
  , { stroke : [withCtrl $ initKey "x", initKey "k"], box : AX.actionCloseCurrentTab }
  , { stroke : [withCtrl $ initKey "c", initKey "f"], box : AX.semiActionShiftTabFocus 1 }
  , { stroke : [withCtrl $ initKey "c", initKey "s"], box : AX.semiActionShiftTabFocus (-1) }
  , { stroke : [withCtrl $ initKey "l"], box : AS.actionShowNavigationShotbox }
  ]
-- prevent non alt/ctrl/meta modifired key on <input>/<textarea>


shrink :: Int -> Key -> Array ActRecord -> Array ActRecord
shrink n k records =
  Array.filter (\r -> maybe false ((==) k) (Array.head (Array.drop n r.stroke))) records

only :: forall a. Array a -> Maybe a
only arr =
  case arr of
    [x] -> Just x
    _ -> Nothing


driveEntry :: Window -> Event -> Effect Unit
driveEntry w e = do
  b <- _.active <$> getGlobal
  if b
    then drive w e
    else pure unit


drive :: Window -> Event -> Effect Unit
drive window event =
  let
    fromEventElement = HTMLElement.fromEventTarget <=< Event.target
    next ns cs = do
      st <- getGlobal
      killTimer st.strokeTimer
      setGlobal (st { stroke = ns, records = cs })
      time <- _.strokeLifetime <$> getConfig
      tid <- Timer.setTimeout time expireStroke
      modifyGlobal (_ { strokeTimer = Just tid })
  in
  case Kbd.fromEvent event of
    Just e -> do
      altIsMeta <- _.altIsMeta <$> getConfig
      k <- pure (evnFromKey altIsMeta e)
      st <- getGlobal
      cs <- pure (shrink (Array.length st.stroke) k st.records)
      ns <- pure (st.stroke <> [k])
      Console.log $ strokeToString ns
      case Tuple (only cs) (fromEventElement event) of
        Tuple (Just x) (Just t) -> do
          if x.stroke == ns
            then do
            expireStroke
            Event.preventDefault event
            Event.stopPropagation event
            x.box.func window t
            else next ns cs
        Tuple _ _ -> do
          if Array.null cs
            then do
            expireStroke
            hideIndicator
            else next ns cs
    _ -> pure unit

expireStroke :: Effect Unit
expireStroke = do
  st <- getGlobal
  killTimer st.strokeTimer
  modifyGlobal (_ { stroke = [], records = keymap, strokeTimer = Nothing })

