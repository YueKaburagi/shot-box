

query
  = e:engine space text:plaintext { return {search: e, query: text}; }
  / text:plaintext { return {query: text}; }


engine
  = ":" atom:atom { return atom; }


plaintext
  = text:$(.+) { return text; }

atom
  = '"' atom:$([^"]+) '"' { return atom; }
  / "'" atom:$([^']+) "'" { return atom; }
  / atom:$([^: \r\n\t][^ \r\n\t]*) { return atom; }

space
  = spc:[ \r\n\t]+