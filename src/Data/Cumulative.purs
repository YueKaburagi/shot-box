

module Data.Cumulative where

import Prelude
import Data.Array as Array
import Data.Either (Either(..))
import Data.Identity (Identity)
import Control.Comonad (class Comonad, extract)
import Control.Extend (class Extend)

data Cumulation a b = Cumulation (Array a) b

instance functorCumulation :: Functor (Cumulation a) where
  map f (Cumulation a b) = Cumulation a (f b)
instance applyCumulation :: Apply (Cumulation a) where
  apply (Cumulation x f) (Cumulation y b) = Cumulation (x <> y) (f b)
instance applicativeCumulation :: Applicative (Cumulation a) where
  pure a = Cumulation [] a
instance semigroupCumulation :: Semigroup b => Semigroup (Cumulation a b) where
  append (Cumulation x a) (Cumulation y b) = Cumulation (x <> y) (a <> b)
instance bindCumulation :: Bind (Cumulation a) where
  bind (Cumulation x a) f = cumu $ f a
    where
      cumu (Cumulation y b) = Cumulation (x <> y) b
instance monadCumulation :: Monad (Cumulation a)
instance extendCumulation :: Extend (Cumulation a) where
  extend f c@(Cumulation a _) = Cumulation a (f c)
instance comonadCumulation :: Comonad (Cumulation a) where
  extract (Cumulation a b) = b




class Cumulative k where
  cumulated :: forall a b . k a b -> Array a
  cumulate :: forall a b c . (b -> c -> c) -> k a b -> Cumulation a c -> Cumulation a c
  cumulateM :: forall a b c m . Applicative m => (b -> c -> m c) -> k a b -> Cumulation a c -> m (Cumulation a c)

cumulateAppend :: forall a b c k . Cumulative k => Semigroup c => (b -> c) -> k a b -> Cumulation a c -> Cumulation a c
cumulateAppend f = cumulate (\b c -> f b <> c)
cumulateAppendC :: forall a b c k . Cumulative k => Semigroup c => (b -> Cumulation a c) -> k a b -> Cumulation a c -> Cumulation a c
cumulateAppendC f k = join <<< cumulateM (\b c -> (_ <> c) <$> f b) k


instance cumulativeEither :: Cumulative Either where
  cumulated (Left a) = [a]
  cumulated _ = []
  cumulate _ (Left a) (Cumulation l c) = Cumulation ([a] <> l) c
  cumulate f (Right b) (Cumulation l c) = Cumulation l (f b c)
  cumulateM _ (Left a) (Cumulation l c) = pure $ Cumulation ([a] <> l) c
  cumulateM f (Right b) (Cumulation l c) = Cumulation l <$> f b c
instance cumulativeCumulation :: Cumulative Cumulation where
  cumulated (Cumulation l _) = l
  cumulate f (Cumulation x b) (Cumulation y c) = Cumulation (x <> y) (f b c)
  cumulateM f (Cumulation x b) (Cumulation y c) = Cumulation (x <> y) <$> f b c



cumulateArrM :: forall a b c m k . Cumulative k => Monad m => (b -> m (k a c)) -> Array b -> m (Cumulation a (Array c))
cumulateArrM f = Array.foldM (\x b -> flip (cumulateAppend pure) x <$> f b) (pure [])
cumulateArrR :: forall a b c k . Cumulative k => (b -> k a c) -> Array b -> Cumulation a (Array c)
cumulateArrR f = extract <<< cumulateArrM (lift f)
  where
    lift :: forall a b . (a -> b) -> a -> Identity b
    lift g a = pure (g a)
cumulateArrL :: forall a b c k . Cumulative k => (b -> k a c) -> Array b -> Cumulation a (Array c)
cumulateArrL f = Array.foldl (\x b -> cumulateAppend pure (f b) x) (pure [])


arCum :: forall a b . Semigroup (Cumulation a b) => Array (Cumulation a b) -> Cumulation a (Array b)
arCum = Array.foldl (\l a -> map pure a <> l) (pure [])

