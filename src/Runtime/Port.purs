
module Runtime.Port where

import Prelude
import Effect (Effect)
import Data.Maybe (Maybe(..))

import Message (BgMessage(..), BgAction(..), BgResult(..), PlainMessage, decode, encode)

import Log as Log

foreign import data Port :: Type


foreign import connectSimple :: Effect Port

foreign import postMessageImpl :: forall r . Port -> PlainMessage -> Effect Unit
postMessage :: Port -> BgMessage -> Effect Unit
postMessage p r = postMessageImpl p (encode r)
postResult :: Port -> BgResult -> Effect Unit
postResult p r = postMessage p (BgRes r)
postAction :: Port -> BgAction -> Effect Unit
postAction p r = postMessage p (BgAct r)

foreign import equalPort :: Port -> Port -> Effect Boolean

foreign import disconnectPort :: Port -> Effect Unit


foreign import data OnMessageListener :: Type
foreign import genOnMessageListenerImpl :: forall a . (Port -> PlainMessage -> Effect a)
                                    -> Effect OnMessageListener
genOnMessageListener :: (Port -> BgMessage -> Effect Unit)
                     -> Effect OnMessageListener
genOnMessageListener f =
  genOnMessageListenerImpl (
    \p r ->
    case decode r of 
      Just x -> f p x
      Nothing -> Log.withErrorShow "bgs-decode" r
    )
foreign import addPortListenerOnMessage :: OnMessageListener -> Port -> Effect Unit
foreign import data OnDisconnectListener :: Type
foreign import genOnDisconnectListener :: forall a . (Port -> Port -> Effect a)
                                       -> Effect OnDisconnectListener
foreign import addPortListenerOnDisconnect :: OnDisconnectListener -> Port -> Effect Unit


