/** eslint hint **/
/* global exports */

"use strict";

exports.connectSimple = function () {
  return browser.runtime.connect();
};

exports.postMessageImpl = function (port) {
  return function (e) {
    return function () {
      port.postMessage(e);
    };
  };
};

exports.equalPort = function (port) {
  return function (p) {
    return function () {
      return port === p;
    };
  };
};
exports.disconnectPort = function (port) {
  return function () {
    port.disconnect();
  };
};



exports.genOnMessageListenerImpl = function (func) {
  return function () {
    return function (port) {
      return function (e) {
        return func(port)(e)();
      };
    };
  };
};
exports.addPortListenerOnMessage = function (func) {
  return function (port) {
    return function () {
      port.onMessage.addListener(func(port));
    };
  };
};

exports.genOnDisconnectListener = function (func) {
  return function () {
    return function (port) {
      return function (p) {
        return func(port)(p)();
      };
    };
  };
};
exports.addPortListenerOnDisconnect = function (func) {
  return function (port) {
    return function () {
      port.onDisconnect.addListener(func(port));
    };
  };
};

