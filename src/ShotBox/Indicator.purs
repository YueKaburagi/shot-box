
module Shotbox.Indicator (addIndicator, showIndicator, hideIndicator) where


import Prelude

import Effect (Effect)
import Effect.Timer as Timer
import Data.Maybe (Maybe(..))

import Web.HTML.HTMLElement as HTMLElement
import Web.HTML.HTMLSpanElement as HTMLSpanElement
import Web.HTML.HTMLDocument (HTMLDocument)
import Web.HTML.HTMLDocument as HTMLDocument
import Web.DOM.Node as DOMNode
import Web.DOM.Element as DOMElement
import Web.DOM.Document as DOMDocument

import State (getGlobal, modifyGlobal)
import Configure (getConfig)
import Util.Timer (killTimer)
import Log as Log

addIndicator :: HTMLDocument -> Effect Unit
addIndicator doc = do
  e <- DOMDocument.createElement "span" (HTMLDocument.toDocument doc)
  DOMElement.setId "webext-shotbox-indicator" e
  body <- HTMLDocument.body doc
  case body of
    Just x -> do
      _ <- DOMNode.appendChild (DOMElement.toNode e) (HTMLElement.toNode x)
      modifyGlobal (_ { indicatorInstance = HTMLSpanElement.fromElement e})
      pure unit
    Nothing -> Log.withErrorShow "page-addIndicator" "no body node"

hideIndicator :: Effect Unit
hideIndicator = do
  st <- getGlobal
  killTimer st.indicatorTimer
  modifyGlobal (_ { indicatorTimer = Nothing })
  case st.indicatorInstance of
    Just indicator -> do
      DOMElement.setAttribute "style" "display: none;" (HTMLSpanElement.toElement indicator)
      DOMNode.setTextContent "" (HTMLSpanElement.toNode indicator)
    Nothing -> Log.withErrorShow "page-hideIndicator" "no indicator instance"

expireIndicator :: Effect Unit
expireIndicator = hideIndicator

showIndicator :: String -> Effect Unit
showIndicator str = do
  st <- getGlobal
  case st.indicatorInstance of
    Just indicator -> do
      DOMElement.setAttribute "style" "bottom: 0px; left: 0px;" (HTMLSpanElement.toElement indicator)
      DOMNode.setTextContent str (HTMLSpanElement.toNode indicator)
      killTimer st.indicatorTimer
      time <- _.indicatorLifetime <$> getConfig
      tid <- Timer.setTimeout time expireIndicator
      modifyGlobal (_ { indicatorTimer = Just tid })
    Nothing -> Log.withErrorShow "page-showIndicator" "no indicator instance"
