/** eslint hint **/
/* global exports */

"use strict";

exports.locationAssign = function (w) {
  return function (href) {
    return function () {
      w.location.assign(href);
    };
  };
};

exports.openNewTab = function (w) {
  return function (href) {
    return function () {
      w.open(href);
    };
  };
};

