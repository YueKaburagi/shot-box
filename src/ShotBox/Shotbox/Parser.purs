
module Shotbox.Shotbox.Parser where

import Prelude

import Control.Alt ((<|>))
import Effect (Effect)
import Effect.Console as Console
import Data.Maybe (Maybe(..))
import Data.Either (Either(..))
import Data.Functor (void)
import Data.Foldable (any, intercalate, class Foldable)
import Data.Array as Array
import Data.List as List
import Data.String.Regex (Regex)
import Data.String.CodeUnits (fromCharArray)
import Data.String.Pattern (Pattern(..), Replacement(..))
import Data.String.Common as String
import Text.Parsing.Parser (ParserT, fail)
import Text.Parsing.Parser.String (anyChar, char, string, oneOf, eof, noneOf)
import Text.Parsing.Parser.Combinators as Parser



type StrParser a = ParserT String Effect a


data NavControl
  = Search Boolean (Array String)
derive instance eqNavControl :: Eq NavControl
instance showNavControl :: Show NavControl where
  show (Search b arr) = "Search " <> show b <> " " <> show arr



parserNav :: Boolean -> StrParser NavControl
parserNav defaultPM = (Search <$> plmin <*> args) <* dropSpace
  where
    woe_ :: StrParser Unit
    woe_ = Parser.choice [whiteSpace_, eof]
    delimited = toStr <$> Parser.manyTill anyChar (char '"')
    atomB = char '"' *> delimited <* dropSpace
    atomA = toStr <$> Parser.many1Till anyChar woe_
    atom :: StrParser String
    atom = atomB <|> atomA
    args = dropSpace *> (Array.fromFoldable <$> Parser.many1Till atom woe_)
    plus = const true <$> (string "+" <|> string "＋")
    minus = const false <$> (string "-" <|> string "－")
    plmin = dropSpace *> (plus <|> minus <|> pure defaultPM)
    dropSpace = Parser.optional whiteSpace


toStr :: forall t . Foldable t => t Char -> String
toStr = fromCharArray <<< Array.fromFoldable
whiteSpace :: StrParser String
whiteSpace = fromCharArray <$> Array.some (oneOf [' ','\r','\n','\t','　'])
whiteSpace_ :: StrParser Unit
whiteSpace_ = void whiteSpace



data Constraint
  = Through
  | ReStrCold String
  | ReStrHot String Regex
data StrGenAtom
  = Text String
  | Url Constraint
  | Title Constraint
-- Selection | Clipboard

instance eqConstraint :: Eq Constraint where
  eq Through Through = true
  eq (ReStrCold a) (ReStrCold b) = a == b
  eq (ReStrHot x _) (ReStrHot y _) = x == y -- Regex has not Eq instance
  eq _ _ = false
derive instance eqStrGenAtom :: Eq StrGenAtom


instance showConstraint :: Show Constraint where
  show (Through) = "Through"
  show (ReStrCold src) = "ReStr " <> src
  show (ReStrHot src re) = "ReStr " <> src <> " .re. " <> show re
instance showStrGenAtom :: Show StrGenAtom where
  show (Text s) = "Text " <> s
  show (Url c) = "Url (" <> show c <> ")"
  show (Title c) = "Title (" <> show c <> ")"

modifyConstraint :: forall m . Functor m => Applicative m => (Constraint -> m Constraint) -> StrGenAtom -> m StrGenAtom
modifyConstraint f (Url c) = Url <$> f c
modifyConstraint f (Title c) = Title <$> f c
modifyConstraint _ s = pure s


-- can i change StrParser to ParserT String Identity ?
parserStrGen :: Char -> Char -> StrParser (Array StrGenAtom)
parserStrGen qS qE = Array.fromFoldable <$> Parser.many1Till (arg <|> text) eof
  where
    genArg :: String -> Constraint -> StrParser StrGenAtom
    genArg str c =
      case str of
        "title" -> pure $ Title c
        "url" -> pure $ Url c
        x -> fail ("unexpected argument " <> x)
    constraint = ReStrCold <$> delimited
    separated = toStr <$> Parser.manyTill (noneOf [qE]) (char ':')
    delimited :: StrParser String
    delimited = toStr <$> Parser.manyTill anyChar (char qE)
    withConstraint = genArg <$> separated <*> constraint
    through = genArg <$> delimited <*> pure Through
    arg = char qS *> join (Parser.try withConstraint <|> through)
    woe_ = eof <|> (void $ Parser.lookAhead $ char qS)
    text = (Text <<< toStr) <$> Parser.many1Till (noneOf [qE]) woe_
      


