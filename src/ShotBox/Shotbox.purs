
module Shotbox.Shotbox (addShotbox, showShotbox, hideShotbox,
                        addTextpane, showTextpane, hideTextpane) where

import Prelude

import Control.Alt ((<|>))
import Effect (Effect)
import Effect.Console as Console
import Data.Maybe (Maybe(..))
import Data.Either (Either(..))
import Data.Functor (void)
import Data.Foldable (any, intercalate)
import Data.Array as Array
import Data.List as List
import Data.String.CodeUnits (fromCharArray)
import Data.String.Pattern (Pattern(..), Replacement(..))
import Data.String.Common as String
import Text.Parsing.Parser (runParserT)

import Web.HTML as HTML
import Web.HTML.Window (Window)
import Web.HTML.HTMLElement as HTMLElement
import Web.HTML.HTMLDocument (HTMLDocument)
import Web.HTML.HTMLDocument as HTMLDocument
import Web.HTML.HTMLInputElement as HTMLInputElement
import Web.HTML.HTMLTextAreaElement as HTMLTextAreaElement

import Web.Event.EventTarget (eventListener, addEventListener)
import Web.Event.Event (EventType(..), Event)
import Web.Event.Event as Event
import Web.UIEvent.KeyboardEvent as Kbd

import Web.DOM.Node as DOMNode
import Web.DOM.Element as DOMElement
import Web.DOM.Document as DOMDocument

import Action (withPort,callbackAcceptSucceed)
import Runtime.Port (postAction)
import Message (BgAction(..))
import State (getGlobal, setGlobal, modifyGlobal)
import Configure (getConfig)
import KeyStroke (ActRecord, Key, evnFromKey, initKey, withCtrl, withMeta)
import Shotbox.Shotbox.Parser (parserNav, NavControl(..))

import Log as Log

listenerImpl :: Event -> Effect Unit
listenerImpl event = do
  case Kbd.fromEvent event of
    Just e -> do
      altIsMeta <- _.altIsMeta <$> getConfig
      k <- pure (evnFromKey altIsMeta e)
      if any ((==) k) [ initKey "Enter", withCtrl $ initKey "m" ]
        then do
        st <- getGlobal
        case st.shotboxInstance of
          Just shotbox -> do
            str <- HTMLInputElement.value shotbox
            if String.null str then pure unit else do
              Event.preventDefault event
              Event.stopPropagation event
              fireShotbox str
          Nothing -> Log.withErrorShow "page-listener-shotbox" "no shotbox"
        else pure unit
    Nothing -> pure unit

-- on Navigation mode
fireShotbox :: String -> Effect Unit
fireShotbox "" = pure unit
fireShotbox s = do
  et <- runParserT s (parserNav false)
  case et of
    Right (Search target args) -> do
      engine <- _.defaultSearchEngine <$> getConfig
      Log.withDebugShow "page-fireShotbox" args
      query <- pure $ intercalate " " args
      url <- pure $ String.replace (Pattern "<>") (Replacement query) engine.urlpat
      w <- HTML.window
      -- a window given from outer ?
      hideShotbox
      if not target
        then locationAssign w url
        else do
        Log.withDebugShow "page-fireShotbox" args
        withPort "page-fireShotbox" (
          \port -> do
            modifyGlobal (_ { callback = callbackAcceptSucceed "page-fireShotbox" })
            postAction port (BAOpenNewTab url)
          )
    Left err -> Log.withErrorShow "page-fireShotbox" err

foreign import locationAssign :: Window -> String -> Effect Unit
foreign import openNewTab :: Window -> String -> Effect Unit



-- Window?
addShotbox :: HTMLDocument -> Effect Unit
addShotbox doc = do
  e <- DOMDocument.createElement "input" (HTMLDocument.toDocument doc)
  DOMElement.setId "webext-shotbox-shotbox" e
  DOMElement.setAttribute "style" "display: none;" e
  body <- HTMLDocument.body doc
  case body of
    Just x -> do
      _ <- DOMNode.appendChild (DOMElement.toNode e) (HTMLElement.toNode x)
      modifyGlobal (_ { shotboxInstance = HTMLInputElement.fromElement e })
      l <- eventListener listenerImpl
      addEventListener (EventType "keydown") l false (HTMLElement.toEventTarget x)
      bl <- eventListener (const hideShotbox)
      addEventListener (EventType "blur") bl false (HTMLElement.toEventTarget x)
    Nothing -> Log.withErrorShow "page-addShotbox" "no body node"
-- onBlur drop?

-- argument MODE?(search/fire-action/find-text)
showShotbox :: String -> Effect Unit
showShotbox str = do
  st <- getGlobal
  case st.shotboxInstance of
    Just shotbox -> do
      DOMElement.setAttribute "style" "top: 460px; left: 460px;" (HTMLInputElement.toElement shotbox)
      HTMLInputElement.setValue str shotbox
      HTMLInputElement.setPlaceholder "Omnibar" shotbox
      HTMLElement.focus (HTMLInputElement.toHTMLElement shotbox)
    Nothing -> Log.withErrorShow "page-showShotbox" "no shotbox instance"

hideShotbox :: Effect Unit
hideShotbox = do
  st <- getGlobal
  case st.shotboxInstance of
    Just shotbox -> do
      DOMElement.setAttribute "style" "display: none;" (HTMLInputElement.toElement shotbox)
      HTMLInputElement.setValue "" shotbox
    Nothing -> Log.withErrorShow "page-hideShotbox" "no shotbox instance"



addTextpane :: HTMLDocument -> Effect Unit
addTextpane doc = do
  e <- DOMDocument.createElement "textarea" (HTMLDocument.toDocument doc)
  DOMElement.setId "webext-shotbox-textpane" e
  DOMElement.setAttribute "style" "display: none;" e
  body <- HTMLDocument.body doc
  case body of
    Just x -> do
      _ <- DOMNode.appendChild (DOMElement.toNode e) (HTMLElement.toNode x)
      modifyGlobal (_ { textpaneInstance = HTMLTextAreaElement.fromElement e })
    Nothing -> Log.withErrorShow "page-addTextpane" "no body node"

showTextpane :: String -> Effect Unit
showTextpane str = do
  st <- getGlobal
  case st.textpaneInstance of
    Just textpane -> do
      DOMElement.setAttribute "style" "" (HTMLTextAreaElement.toElement textpane)
      HTMLTextAreaElement.setValue str textpane
    Nothing -> Log.withErrorShow "page-showTextpane" "no textpane instance"

hideTextpane :: Effect Unit
hideTextpane = do
  st <- getGlobal
  case st.textpaneInstance of
    Just textpane -> do
      DOMElement.setAttribute "style" "display: none;" (HTMLTextAreaElement.toElement textpane)
      HTMLTextAreaElement.setValue "" textpane
    Nothing -> Log.withErrorShow "page-hideTextpane" "no textpane instance"
