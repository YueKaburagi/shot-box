/** eslint hint **/
/* global exports */

"use strict";


exports.setGlobal = function (data) {
  return function() {
    global = data;
  };
};
exports.getGlobal = function () {
  return global;
};
