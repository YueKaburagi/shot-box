module Background (bgmain) where

import Prelude

import Effect (Effect)
import Effect.Promise (Promise, runPromise)
import Data.Maybe (Maybe(..))
import Data.Nullable (toMaybe)
import Data.Array as Array
import Control.Error.Util (bool)
import Data.Map (Map)
import Data.Map as Map

import Background.Tab (Tab)
import Background.Tab as Tab
import Log (withErrorShow)
import Log as Log
import Message (BgMessage(..), BgAction(..), BgResult(..), mkTabInfo)
import Runtime.Port (Port, postResult, equalPort, disconnectPort, genOnMessageListener, addPortListenerOnMessage, genOnDisconnectListener, addPortListenerOnDisconnect)
import Background.Listener 
import Background.Types

bgmain :: Effect Unit
bgmain = do
  setGlobal initState
  addBrowserActionListenerOnClick =<< genBrowserActionListener toggleIcon
  addRuntimeListenerOnConnect =<< genOnConnectListener onConnect
  addTabsListenerOnActivated =<< genOnActivatedListener onActive
  addTabsListenerOnAttached =<< genOnAttachedListener onAttached
  addTabsListenerOnUpdated =<< genOnUpdatedListener onUpdated
  addTabsListenerOnFocusChanged =<< genOnFocusChangedListener onFocusChanged
  Log.withLogShow "bgs" "initialized"

type BackgroundState =
  { activeWindow :: Maybe WindowIndex
  , knownWindows :: Map WindowIndex (Maybe TabIndex)
  , connections :: Array Port
  , active :: Boolean
  }
initState :: BackgroundState
initState =
  { activeWindow : Nothing
  , knownWindows : Map.empty
  , connections : []
  , active : true
  }

foreign import getGlobal :: Effect BackgroundState
foreign import setGlobal :: BackgroundState -> Effect Unit
modifyGlobal :: (BackgroundState -> BackgroundState) -> Effect Unit
modifyGlobal f = setGlobal =<< (f <$> getGlobal)
getActiveTab :: Effect (Maybe TabIndex)
getActiveTab = do
  s <- getGlobal
  case s.activeWindow of
    Just wid ->
      pure (join $ Map.lookup wid s.knownWindows)
    Nothing ->
      pure Nothing



-- | see [https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/browserAction/setIcon]
foreign import setBrowserActionIcon :: forall r . { | r } -> Effect (Promise Unit)
type BrowserActionIcon r = { path :: { "19" :: String, "38" :: String } | r }
activeIcon :: TabIndex -> BrowserActionIcon ( tabId :: TabIndex )
activeIcon = { path : { "19" : "icons/dummy-o.png", "38" : "icons/dummy-o-2x.png" }, tabId : _ }
inactiveIcon :: TabIndex -> BrowserActionIcon ( tabId :: TabIndex )
inactiveIcon = { path : { "19" : "icons/dummy-x.png", "38" : "icons/dummy-x-2x.png" }, tabId : _ }
setIcon :: Tab -> (TabIndex -> BrowserActionIcon ( tabId :: TabIndex )) -> Effect Unit
setIcon tab icon = do
  tid <- Tab.index tab
  p <- setBrowserActionIcon $ icon tid
  runPromise (\_ -> pure unit ) (Log.withErrorShow "bgs-setIcon") p
toggleIcon :: Tab -> BrowserActionOnClickedDetails -> Effect Unit
toggleIcon tab _ = do
  modifyGlobal (\o -> o { active = not o.active })
  setCurrentIcon tab
setCurrentIcon :: Tab -> Effect Unit
setCurrentIcon tab = do
  b <- _.active <$> getGlobal
  setIcon tab $ bool inactiveIcon activeIcon b


onConnect :: Port -> Effect Unit
onConnect port = do
  modifyGlobal (\s -> s { connections = s.connections <> [port] })
  discL <- genOnDisconnectListener onDisconnect 
  addPortListenerOnDisconnect discL port
  hearL <- genOnMessageListener onMessage
  addPortListenerOnMessage hearL port
  Log.withDebugShow "bgs-onConnect" "connected"
-- is port as p ? when port.onDisconnect((p) => {});
onDisconnect :: Port -> Port -> Effect Unit
onDisconnect port p = do
  st <- getGlobal
  conns <- Array.filterA (equalPort port) st.connections
  setGlobal (st { connections = conns })


onActive :: ActiveInfo -> Effect Unit
onActive e = do
  modifyGlobal (\x -> x
                 { activeWindow = Just e.windowId
                 , knownWindows = Map.insert e.windowId (Just e.tabId) x.knownWindows })

onAttached :: Int -> AttachInfo -> Effect Unit
onAttached tabIx attach = do
  modifyGlobal (\x -> x
                 { activeWindow = Just attach.newWindowId
                 , knownWindows = Map.insert attach.newWindowId (Just tabIx) x.knownWindows })

onUpdated :: Int -> Tab -> Effect Unit
onUpdated tabIx tab = do
  -- `open in new tab (background)` fires onUpdated, but such tab is not active.
  isActive <- Tab.active tab
  if isActive 
    then do
      wid <- Tab.windowId tab
      modifyGlobal (\x -> x
                     { activeWindow = Just wid
                     , knownWindows = Map.insert wid (Just tabIx) x.knownWindows })
    else pure unit

onFocusChanged :: WindowIndex -> Effect Unit
onFocusChanged wid = do
  modifyGlobal (_ { activeWindow = Just wid })



foreign import fireTabsGet :: TabIndex -> Effect (Promise Tab)
foreign import fireTabsQuery :: forall r . { | r } -> Effect (Promise (Array Tab))
foreign import fireTabsRemove :: TabIndex -> Effect (Promise Unit)
foreign import fireTabsUpdate :: forall r . TabIndex -> { | r } -> Effect (Promise Tab)
foreign import fireTabsCreate :: forall r . { | r } -> Effect (Promise Tab)

onMessage :: Port -> BgMessage -> Effect Unit
onMessage port (BgAct a) = invokeAction port a
onMessage _ x = Log.withDebugShow "bgs-onMessage" x

invokeAction :: Port -> BgAction -> Effect Unit
invokeAction port BAGetCurrentTabInfo = do
  Log.withDebugShow "bgs-getCurrentTabinfo" "on BAGetCurrentTabInfo"
  t <- getActiveTab
  case t of
    Just ix -> do
      p <- fireTabsGet ix
      runPromise (
        \tab -> do
          info <- mkTabInfo
                  <$> Tab.title tab
                  <*> Tab.url tab
                  <*> Tab.faviconUrl tab
                  <*> Tab.height tab
                  <*> Tab.width tab
          postResult port $ BRResultGetCurrentTabInfo info
          ) (withErrorShow "bgs-getCurrentTabinfo") p
    _ -> do
      postResult port $ BRFailed "no active tab"
invokeAction port BACloseCurrentTab = do
  Log.withDebugShow "bgs-getCurrentTabinfo" "on BACloseCurrentTab"
  t <- getActiveTab
  case t of
    Just ix -> do
      p <- fireTabsRemove ix
      runPromise (\_ -> disconnectPort port) (withErrorShow "bgs-closeCurrentTab") p
    _ -> withErrorShow "bgs-closeCurrentTab" "no active tab"
invokeAction port (BAShiftTabFocus offset) = do
  Log.withDebugShow "bgs-getShiftTabFocus" "on BAShiftTabFocus"
  t <- getActiveTab
  case t of
    Just ix -> do
      p0 <- fireTabsGet ix
      runPromise (
        \tab -> do
          q <- (\a b -> {index: max 0 (a + offset), windowId: b}) <$> Tab.index tab <*> Tab.windowId tab
          p1 <- fireTabsQuery q
          runPromise (
            \tabs ->
            case Array.uncons tabs of
              Just { head: x, tail: xs }
                | not (Array.null xs) ->
                  withErrorShow "bgs-shiftTabFocus" "too many candidates"
                | otherwise -> do
                  tix <- toMaybe <$> Tab.id x
                  case tix of
                    Just c -> do
                      p2 <- fireTabsUpdate c ({active: true})
                      runPromise (\_ -> pure unit) (withErrorShow "bgs-shiftTabFocus") p2
                    Nothing -> withErrorShow "bgs-shiftTabFocus" "missing tab id"
              Nothing -> withErrorShow "bgs-shiftTabFocus" "no candidates"
            ) (withErrorShow "bgs-shiftTabFocus") p1
        ) (withErrorShow "bgs-shiftTabFocus") p0
    _ -> withErrorShow "bgs-shiftTabFocus" "no active tab"
  -- !!!! DIRTY CODE !!!!
invokeAction port (BAOpenNewTab u) = do
  Log.withDebugShow "bgs-openNewTab" "on BAOpenNewTab"
  t <- getActiveTab
  case t of
    Just ix -> do
      p <- fireTabsCreate { url : u, openerTabId : ix }
      runPromise (\_ -> postResult port BRSucceed) (withErrorShow "bgs-openNewTab") p
    Nothing -> withErrorShow "bgs-openNewTab" "no active tab"
invokeAction port (BAGetStateActive) = do
  Log.withDebugShow "bgs-getStateActive" "on BAGetStateActive"
  s <- getGlobal
  -- should i fix as promise ?
  postResult port (BRResultActive s.active)
invokeAction port (BAToggleStateActive) = do
  Log.withDebugShow "bgs-toggleStateActive" "on BAToggleStateActive"
  modifyGlobal (\s -> s { active = not s.active })
  s <- getGlobal
  postResult port (BRResultActive s.active)
