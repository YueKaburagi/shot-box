/*************** eslint hint ***************/
/* global Promise */

/*
// configurations
function mkPass(obj) {
  if (obj.fullList !== undefined) { return obj.fullList.includes; }
  if (obj.range !== undefined) { return (x) => obj.range.min <= x && x <= obj.range.max; }
}

const shotBoxConfigRange = {
  nonDirectionalSelectionEnds: {
    fullList: ["forward","backward"]
  },
  strokeExpiresTime: {
    range: {min: 100, max: 3000}
  }
};

const shotBoxConfig = {
  nonDirectionalSelectionEnds: "forward",
  strokeExpiresTime: 1200,

  shotboxLayout: {width: "580px", height: "26px",
                  anchor: "top-left", top: "460px", left: "460px",
                  margin: "2px", padding: "2px", border: "none"},
  shotboxDesign: {background: "#352637", color: "#fafdfb"},

  global_keymap: [],
  local_keymaps: [
    {url: "^https?://.*\\.nicovideo\\.jp/watch/.*",
     // give url pattern first
     // query cloak().create
     keymap: cloak(() => [
      {stroke: [{key: ' '}], fix: "do nothing",
       predicate: "not isEditableElement",
       minor: "not edit"
       propagation: true, keymap: "bottom-level"}
    ])}
  ]
};
// */

// to fix: current tab change on window clicked

const backgroundResource = "background resource";
const backgroundFunction = (e) => "<<" + e + ">>"; // cannot clone this object
const backgroundPackedFunction = {func: backgroundFunction}; // cannot clone this object

let connections = [];

// how to make a connection already loaded tabs?
browser.runtime.onConnect.addListener( (conn) => {
  connections.push(conn);
  conn.onMessage.addListener(hear(conn));
  conn.onDisconnect.addListener(disconnectAfter(conn));
});
const hear = (conn) => (m) => {
  if (!isDefined(m)) {console.warn("invalidate message:"+m);}
  else {
    undefinedMaybe(
      () => console.warn("unkown.message:"+m),
      () => (op) => op.func(conn)(m.args)
    )(  operations.find( (op) => op.ix == m.msg )  );
  }
};

const disconnectAfter = (conn) => (_) => {
  connections = connections.filter((c) => c !== conn);
}


const status = {activeTabIx: null, activeWindowIx: null};
browser.tabs.onActivated.addListener( (e) => {
  status.activeWindowIx = e.windowId;
  status.activeTabIx = e.tabId;
});
browser.tabs.onUpdated.addListener( (ix,_,tab) => {
  // `open in new tab (behind)` fires onUpdated, but such tab is inactive.
  if (tab.active) {
    status.activeTabIx = ix;
    status.activeWindowIx = tab.windowId;
  }
});
browser.tabs.onAttached.addListener( (ix,attach) => {
  status.activeTabIx = ix;
  status.activeWindowIx = attach.newWindowId;
});



const getCurrentTabInfo = (conn) => (_) => { /* _ -> getCurrentTabInfo, {title,url,favionUrl,height,width} */
  browser.tabs.get(status.activeTabIx).then(
    (tab) => {
      const r = {};
      r.title = undefinedOptionS("!no title!")(tab.title);
      r.url = undefinedOptionS("!no url!")(tab.url);
      r.faviconUrl = undefinedOptionS("!no faviconurl!")(tab.favIconUrl);
      r.height = tab.height;
      r.width = tab.width;
      conn.postMessage({ix: "getCurrentTabInfo", data: r});
    }
  )
};
const closeCurrentTab = (conn) => (_) => { /* _ -> () */
  const failed = (e) => {console.warn("[g]close current tab failed: "+e);};

  const _b = browser.tabs.remove(status.activeTabIx);
  _b.then( (_) => conn.disconnect() , failed);
};

const focusTab = (_) => (args) => { /* {offset: int} -> () */
  const offset = args.offset;
  const failed = (e) => {console.warn("[g]select tab failed: "+e);};
  browser.tabs.get(status.activeTabIx).then(
    (tab) => browser.tabs.query(
      {index: Math.max(0, tab.index + offset), // how i get amount of tabs?
       currentWindow: true}
    ).then(
      (tabs) => {
        if (tabs.length == 1) {
          return browser.tabs.update(tabs[0].id, {active: true});
        } else { return new Promise( (s,f) => f("many candidates") ); }
      },failed)
    ,failed);
};




const operations = [
  {ix: "close current tab", func: closeCurrentTab},
  {ix: "get current tab info", func: getCurrentTabInfo},
  {ix: "focus tab", func: focusTab}
];
