
module Configure where

import Prelude

import Effect (Effect)
import Data.Array as Array
import Data.Maybe (Maybe(..))
import Data.String.Regex (Regex, regex)
import Data.String.Regex.Flags (noFlags)
import Data.Either (Either(..), either)
import Data.Tuple (Tuple(..))
import Data.Traversable (traverse)
import Data.Bifunctor (class Bifunctor, lmap, rmap)
import Text.Parsing.Parser (runParserT)

import Shotbox.Shotbox.Parser (StrGenAtom, Constraint(..), modifyConstraint, parserStrGen)
import Log as Log
import Data.Cumulative (Cumulation, cumulate, cumulateAppendC, arCum, cumulateArrL)

type ShotBoxConfigure =
  { strokeLifetime :: Int
  , indicatorLifetime :: Int
  , shotBoxTop :: Int
  , shotBoxLeft :: Int
  , altIsMeta :: Boolean
  , defaultSearchEngine :: { urlpat :: String }
--  <> is replacer
--  url https://www.google.com/search?q=<>
--  url https://duckduckgo.com/?q=<>
--  url https://pursuit.purescript.org/search?q=<>
--  url https://developer.mozilla.org/en-US/search?q=<>
--  url https://ejje.weblio.jp/content_find?searchType=exact&query=<>
--  url http://www.dlsite.com/maniax/fsr/=/language/jp/sex_category[0]/male/keyword/<>
--  url https://packages.gentoo.org/packages/search?q=
--  url https://scryfall.com/search?q=
  , titleWithFixRecords :: Array MalformedTitleWithFixRule
  , strGenMap :: StrGenMapCold
  }
initConfig :: ShotBoxConfigure
initConfig =
  { strokeLifetime : 1200
  , indicatorLifetime : 2600
  , shotBoxTop : 460
  , shotBoxLeft : 460
  , altIsMeta : true
  , defaultSearchEngine : { urlpat : "https://www.google.com/search?q=<>" }
  , titleWithFixRecords : (loadFixRule <$> dummyFixRules)
  , strGenMap : dummyStrGenRules
  }

getConfig :: Effect ShotBoxConfigure
getConfig = pure initConfig



boil :: StrGenMapCold -> Effect (Cumulation String StrGenMapHot)
boil coldMap = arCum <$> traverse boilSheat coldMap -- Ef Ar Cum
  where
    parse :: String -> Tuple Char Char -> Effect (Either String (Array StrGenAtom))
    parse s (Tuple qB qE) = lmap show <$> runParserT s (parserStrGen qB qE)
    boilRec :: StrGenRecordCold -> Effect (Either String (Cumulation String StrGenRecordHot))
    boilRec r = (
      \gEA -> do
        m <- regex r.matcher noFlags
        gA <- gEA
        g <- pure $ boilAtoms gA
        pure (map { matcher : m, generator : _ } g)
      ) <$> parse r.generator r.delimiter
    boilSheat :: StrGenSheatCold -> Effect (Cumulation String StrGenSheatHot)
    boilSheat s = map ({ index : s.index, records : _ }) <$> Array.foldM acc (pure []) s.records
    acc :: Cumulation String (Array StrGenRecordHot) -> StrGenRecordCold -> Effect (Cumulation String (Array StrGenRecordHot))
    acc a b = do
      ec <- boilRec b
      pure $ cumulateAppendC (map pure) ec a
    boilAtoms :: Array StrGenAtom -> Cumulation String (Array StrGenAtom)
    boilAtoms = map Array.reverse <<< cumulateArrL (modifyConstraint boilConstraint)
    boilConstraint :: Constraint -> Either String Constraint
    boilConstraint (ReStrCold s) = ReStrHot s <$> regex s noFlags
    boilConstraint x = pure x
type StrGenRecordCold =
  { matcher :: String
  , delimiter :: Tuple Char Char
  , generator :: String
  }
type StrGenRecordHot =
  { matcher :: Regex
  , generator :: Array StrGenAtom
  }
type StrGenSheat a =
  { index :: String
  , records :: Array a
  }
type StrGenSheatCold = StrGenSheat StrGenRecordCold
type StrGenSheatHot = StrGenSheat StrGenRecordHot
type StrGenMapCold = Array StrGenSheatCold
type StrGenMapHot = Array StrGenSheatHot
dummyStrGenRules :: StrGenMapCold
dummyStrGenRules =
  [ { index : "mk-bu-record"
    , records :
      -- lowest priority
      [ { matcher : ".*"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: <title>\n    records:\n      - name: home\n        url: <url>\n    tags: []"
        }
      , { matcher : "^https?://.+\\.nicovideo\\.jp/watch"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: <title:^(.+) - ニコニコ動画.*$>\n    records:\n      - name: nico\n        url: <url>\n    tags: [video]"
        }
      , { matcher : "^https?://.+\\.nicovideo\\.jp/mylist"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: t\n    records:\n      - name: <title:^「(.+)」.* さんの公開マイリスト.*$>\n        url: <url>\n    tags: [video, src]"
        }
      , { matcher : "^https?://.+\\.nicovideo\\.jp/series"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: t\n    records:\n      - name: <title:^(.+) -.*$>\n        url: <url>\n    tags: [video, src]"
        }
      , { matcher : "^https?://.+\\.nicovideo\\.jp/user/.+/video"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: <title:^(.+) - niconico.*$>\n    records:\n      - name: nico-u\n        url: <url>\n    tags: [video, src]"
        }
      , { matcher : "^https?://seiga\\.nicovideo\\.jp/user/illust"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: <title:^(.+) さんのイラスト一覧.*$>\n    records:\n      - name: nico-s\n        url: <url>\n    tags: [illust, src, illustrator]"
        }
      , { matcher : "^https?://.+\\.pixiv\\.net/users/.+"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: <title:^(.+) - pixiv.*$>\n    records:\n      - name: xiv-u\n        url: <url>\n    tags: [illust, src, illustrator]"
        }
      , { matcher : "^https?://.+\\.pixiv\\.net/artworks/"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: <title:^.*\\s+(.+) - .+の.*$>\n    records:\n      - name: xiv-i\n        url: <url>\n    tags: [illust]"
        }
      , { matcher : "^https?://seiga\\.nicovideo\\.jp/seiga"
        , delimiter : Tuple '<' '>'
        , generator : "  - title: <title:^(.+) / .+ さんのイラスト.*$>\n    records:\n      - name: nico-i\n        url: <url>\n    tags: [illust]"
        }
        -- highest priority
      ]
    }
  ]


type MalformedTitleWithFixRule =
  { urlS :: String
  , urlR :: Maybe Regex
  , titleS :: String
  , titleR :: Maybe Regex
  }
dummyFixRules :: Array MalformedTitleWithFixRule
dummyFixRules =
  [ { urlS : "^https?://.+\\.nicovideo\\.jp/watch"
    , titleS : "^(.+) - ニコニコ動画.*$"
    , urlR : Nothing, titleR : Nothing
    }
  , { urlS : "^https?://.+\\.nicovideo\\.jp/mylist"
    , titleS : "^「(.+)」.* さんの公開マイリスト.*$"
    , urlR : Nothing, titleR : Nothing
    }
  , { urlS : "^https?://.+\\.nicovideo\\.jp/user/.+/video"
    , titleS : "^(.+) - niconico.*$"
    , urlR : Nothing, titleR : Nothing
    }
  , { urlS : "^https?://seiga\\.nicovideo\\.jp/user/illust"
    , titleS : "^(.+) さんのイラスト一覧.*$"
    , urlR : Nothing, titleR : Nothing
    }
  , { urlS : "^https?://.+\\.pixiv\\.net/member\\.php\\?id="
    , titleS : "^(.+) - pixiv.*$"
    , urlR : Nothing, titleR : Nothing
    }
  , { urlS : "^https?://.+\\.pixiv\\.net/artworks/"
    , titleS : "^.*\\s+(.+) - .+の.*$"
    , urlR : Nothing, titleR : Nothing
    }
  , { urlS : "^https?://seiga\\.nicovideo\\.jp/seiga"
    , titleS : "^(.+) / .+ さんのイラスト.*$"
    , urlR : Nothing, titleR: Nothing
    }
  ]
loadFixRule :: MalformedTitleWithFixRule -> MalformedTitleWithFixRule
loadFixRule rule =
  rule { urlR = either (const Nothing) Just (regex rule.urlS noFlags)
       , titleR = either (const Nothing) Just (regex rule.titleS noFlags)
       }
