/** eslint hint **/
/* global exports */

"use strict";

exports.active = function (tab) {
  return function () {
    return tab.active;
  };
};
exports.index = function (tab) {
  return function () {
    return tab.index;
  };
};
exports.windowId = function (tab) {
  return function () {
    return tab.windowId;
  };
};

exports.idImpl = function (tab) {
  return function () {
    return tab.id;
  };
};
exports.titleImpl = function (tab) {
  return function () {
    return tab.title;
  };
};
exports.urlImpl = function (tab) {
  return function () {
    return tab.url;
  };
};
exports.faviconUrlImpl = function (tab) {
  return function () {
    return tab.faviconUrl;
  };
};
exports.heightImpl = function (tab) {
  return function () {
    return tab.height;
  };
};
exports.widthImpl = function (tab) {
  return function () {
    return tab.width;
  };
};
