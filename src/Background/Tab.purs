
module Background.Tab ( Tab
                      , active, index, windowId
                      , id, title, url, faviconUrl, height, width
                      ) where

import Prelude (map, (<<<))

import Effect (Effect)
import Data.Nullable (Nullable)

-- Nullable makes no distinction between null and undefined.

foreign import data Tab :: Type


foreign import active :: Tab -> Effect Boolean
foreign import index :: Tab -> Effect Int
foreign import windowId :: Tab -> Effect Int


foreign import idImpl :: Tab -> Effect (Nullable Int)
id :: Tab -> Effect (Nullable Int)
id = idImpl
foreign import titleImpl :: Tab -> Effect (Nullable String)
title :: Tab -> Effect (Nullable String)
title = titleImpl
foreign import urlImpl :: Tab -> Effect (Nullable String)
url :: Tab -> Effect (Nullable String)
url = urlImpl
foreign import faviconUrlImpl :: Tab -> Effect (Nullable String)
faviconUrl :: Tab -> Effect (Nullable String)
faviconUrl = faviconUrlImpl
foreign import heightImpl :: Tab -> Effect (Nullable Int)
height :: Tab -> Effect (Nullable Int)
height = heightImpl
foreign import widthImpl :: Tab -> Effect (Nullable Int)
width :: Tab -> Effect (Nullable Int)
width = widthImpl

