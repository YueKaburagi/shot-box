module Background.Listener where

import Prelude

import Effect (Effect)

import Background.Tab (Tab)

import Runtime.Port (Port)
import Background.Types (WindowIndex)

type BrowserActionOnClickedDetails =
  { modifires :: Array String
  , button :: Int
  }
foreign import data BrowserActionListener :: Type
foreign import genBrowserActionListener :: forall a . (Tab -> BrowserActionOnClickedDetails -> Effect a)
                                        -> Effect BrowserActionListener
foreign import addBrowserActionListenerOnClick :: BrowserActionListener -> Effect Unit


foreign import data OnConnectListener :: Type
foreign import genOnConnectListener :: forall a . (Port -> Effect a)
                                    -> Effect OnConnectListener
foreign import addRuntimeListenerOnConnect :: OnConnectListener -> Effect Unit


type ActiveInfo =
  { previousTabId :: Int
  , tabId :: Int
  , windowId :: Int
  }
foreign import data OnActivatedListener :: Type
foreign import genOnActivatedListener :: forall a . (ActiveInfo -> Effect a)
                                      -> Effect OnActivatedListener
foreign import addTabsListenerOnActivated :: OnActivatedListener -> Effect Unit


type AttachInfo =
  { newWindowId :: Int
  , newPosition :: Int
  }
foreign import data OnAttachedListener :: Type
foreign import genOnAttachedListener :: forall a . (Int -> AttachInfo -> Effect a)
                                     -> Effect OnAttachedListener
foreign import addTabsListenerOnAttached :: OnAttachedListener -> Effect Unit


foreign import data OnUpdatedListener :: Type
foreign import genOnUpdatedListener :: forall a . (Int -> Tab -> Effect a)
                                    -> Effect OnUpdatedListener
foreign import addTabsListenerOnUpdated :: OnUpdatedListener -> Effect Unit


foreign import data OnFocusChangedListener :: Type
foreign import genOnFocusChangedListener :: forall a . (WindowIndex -> Effect a)
                                         -> Effect OnFocusChangedListener
foreign import addTabsListenerOnFocusChanged :: OnFocusChangedListener -> Effect Unit
