/** eslint hint **/
/* global exports:readonly */

"use strict";




exports.genBrowserActionListener = function (func) {
  return function () {
    return function (tab, details) {
      return func(tab)(details)();
    };
  };
};
exports.addBrowserActionListenerOnClick = function (listener) {
  return function () {
    browser.browserAction.onClicked.addListener(listener);
  };
};


exports.genOnConnectListener = function (func) {
  return function () {
    return function (port) {
      return func(port)();
    };
  };
};
exports.addRuntimeListenerOnConnect = function (listener) {
  return function () {
    browser.runtime.onConnect.addListener(listener);
  };
};




exports.genOnActivatedListener = function (func) {
  return function () {
    return function (data) {
      return func(data)();
    };
  };
};
exports.addTabsListenerOnActivated = function (listener) {
  return function () {
    browser.tabs.onActivated.addListener(listener);
  };
};


exports.genOnAttachedListener = function (func) {
  return function () {
    return function (ix,attach) {
      return func(ix)(attach)();
    };
  };
};
exports.addTabsListenerOnAttached = function (listener) {
  return function () {
    browser.tabs.onAttached.addListener(listener);
  };
};

exports.genOnUpdatedListener = function (func) {
  return function () {
    return function (ix,_,tab) {
      return func(ix)(tab)();
    };
  };
};
exports.addTabsListenerOnUpdated = function (listener) {
  return function () {
    browser.tabs.onUpdated.addListener(listener);
  };
};

exports.genOnFocusChangedListener = function (func) {
  return function () {
    return function (wix) {
      return func(wix)();
    };
  };
};
exports.addTabsListenerOnFocusChanged = function (listener) {
  return function () {
    browser.windows.onFocusChanged.addListener(listener);
  };
};

