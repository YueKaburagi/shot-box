
module Message where

import Prelude
import Data.Show (show)
import Data.Maybe (Maybe(..))
import Data.Generic.Rep (class Generic)
import Data.Nullable (Nullable, null, notNull, toMaybe)
import Data.Nullable as Nullable

import Test.QuickCheck.Arbitrary (class Arbitrary, arbitrary, genericArbitrary)
import Test.QuickCheck.Gen (Gen)
import Control.Monad.Gen as Gen
import Data.NonEmpty (NonEmpty(..))

data BgAction
  = BAGetCurrentTabInfo
  | BACloseCurrentTab
  | BAShiftTabFocus Int
  | BAOpenNewTab String
  | BAGetStateActive
  | BAToggleStateActive

data BgResult
  = BRResultGetCurrentTabInfo TabInfo
  | BRResultActive Boolean
  | BRSucceed
  | BRFailed String

data BgMessage
  = BgAct BgAction
  | BgRes BgResult


type PlainMessage =
  {  _type :: String
  , act :: Nullable MAction
  , res :: Nullable MResult
  }
initMessage :: String -> PlainMessage
initMessage s = { _type : s, act : null, res : null }
type MAction =
  { _type :: String
  , offset :: Nullable Int
  , url :: Nullable String
  }
initMAction :: String -> MAction
initMAction s = { _type : s, offset : null, url : null }
type MResult =
  { _type :: String
  , tabInfo :: Nullable TabInfo
  , errMsg :: Nullable String
  , bool :: Nullable Boolean
  }
initMResult :: String -> MResult
initMResult s = { _type : s, tabInfo : null, errMsg : null, bool: null }

encode :: BgMessage -> PlainMessage
encode (BgAct a) = (_ { act = notNull (encodeBA a) }) $ initMessage "BgAct"
encode (BgRes r) = (_ { res = notNull (encodeBR r) }) $ initMessage "BgRes"
encodeBA :: BgAction -> MAction
encodeBA (BAGetCurrentTabInfo) = initMAction "BAGetCurrentTabInfo"
encodeBA (BACloseCurrentTab) = initMAction "BACloseCurrentTab"
encodeBA (BAShiftTabFocus n) = (_ { offset = notNull n }) $ initMAction "BAShiftTabFocus"
encodeBA (BAOpenNewTab url) = (_ { url = notNull url }) $ initMAction "BAOpenNewTab"
encodeBA (BAGetStateActive) = initMAction "BAGetStateActive"
encodeBA (BAToggleStateActive) = initMAction "BAToggleStateActive"
encodeBR :: BgResult -> MResult
encodeBR (BRResultGetCurrentTabInfo t) = (_ { tabInfo = notNull t }) $ initMResult "BRResultGetCurrentTabInfo"
encodeBR (BRResultActive b) = (_ { bool = notNull b }) $ initMResult "BRResultActive"
encodeBR (BRSucceed) = initMResult "BRSucceed"
encodeBR (BRFailed s) = (_ { errMsg = notNull s }) $ initMResult "BRFailed"

decode :: PlainMessage -> Maybe BgMessage
decode r =
  case r._type of
    "BgAct" -> BgAct <$> (decodeBA =<< toMaybe r.act)
    "BgRes" -> BgRes <$> (decodeBR =<< toMaybe r.res)
    _ -> Nothing
decodeBA :: MAction -> Maybe BgAction
decodeBA r =
  case r._type of
    "BAGetCurrentTabInfo" -> Just BAGetCurrentTabInfo
    "BACloseCurrentTab" -> Just BACloseCurrentTab
    "BAShiftTabFocus" -> BAShiftTabFocus <$> toMaybe r.offset
    "BAOpenNewTab" -> BAOpenNewTab <$> toMaybe r.url
    "BAGetStateActive" -> Just BAGetStateActive
    "BAToggleStateActive" -> Just BAToggleStateActive
    _ -> Nothing
decodeBR :: MResult -> Maybe BgResult
decodeBR r =
  case r._type of
    "BRResultGetCurrentTabInfo" -> BRResultGetCurrentTabInfo <$> toMaybe r.tabInfo
    "BRResultActive" -> BRResultActive <$> toMaybe r.bool
    "BRSucceed" -> Just BRSucceed
    "BRFailed" -> BRFailed <$> toMaybe r.errMsg
    _ -> Nothing




mkTabInfo :: Nullable String -> Nullable String -> Nullable String
          -> Nullable Int -> Nullable Int -> TabInfo
mkTabInfo = { title : _, url : _, faviconUrl : _, height : _, width : _ }
type TabInfo =
  { title :: Nullable String
  , url :: Nullable String
  , faviconUrl :: Nullable String
  , height :: Nullable Int
  , width :: Nullable Int
  }
genArb :: forall a . Arbitrary a => Gen (Nullable a)
genArb = Nullable.toNullable <$> arbitrary
passArb :: Gen TabInfo
passArb = mkTabInfo <$> genArb <*> genArb <*> genArb <*> genArb <*> genArb

derive instance eqBgAction :: Eq BgAction
derive instance genericBgAction :: Generic BgAction _
instance arbitraryBgAction :: Arbitrary BgAction where
  arbitrary = genericArbitrary
instance showBgAction :: Show BgAction where
  show BAGetCurrentTabInfo = "GetCurrentTabInfo"
  show BACloseCurrentTab = "CloseCurrentTab"
  show (BAShiftTabFocus o) = "ShiftTabFocus " <> show o
  show (BAOpenNewTab url) = "OpenNewTab " <> show url
  show (BAGetStateActive) = "GetStateActive"
  show (BAToggleStateActive) = "ToggleStateActive"

derive instance eqBgResult :: Eq BgResult
derive instance genericBgResult :: Generic BgResult _
instance arbitraryBgResult :: Arbitrary BgResult where
  arbitrary =
    Gen.oneOf (
      NonEmpty (pure BRSucceed)
      [ BRFailed <$> arbitrary
      , BRResultActive <$> arbitrary
      , BRResultGetCurrentTabInfo <$> passArb
      ] )
instance showBgResult :: Show BgResult where
  show (BRResultGetCurrentTabInfo i) = "ResultGetCurrentTabInfo" <> show i
  show (BRResultActive b) = "ResultActive" <> show b
  show (BRSucceed) = "Succeed"
  show (BRFailed r) = "Failed " <> r

derive instance eqBgMessage :: Eq BgMessage
derive instance genericBgMessage :: Generic BgMessage _
instance arbitraryBgMessage :: Arbitrary BgMessage where
  arbitrary = genericArbitrary
instance showBgMessage :: Show BgMessage where
  show (BgAct a) = "BgAct " <> show a
  show (BgRes r) = "BgRes " <> show r
