

// iniitalize content script state with keymap
stateInit(all_keymap);


// edit : document.activeElement

// may resetStroke onEvenet 'click'
// [record] => first success of ?


window.addEventListener('keydown', (e) => {
  let _a = pushKeyToStroke(e);
  debugout(Stroke.showStroke(oneShotState.stroke), 0);
  debugout("keydown.1",3);
  let _b = eitherBind( _const(checkAndShrinkCandidates(e)) )(_a);
  debugout("keydown.2",3);
  let _c = eitherBind( lazy( fireRecord(e) ) )(_b);
  debugout("keydown.3",3);
  debugout( either(ident,() => (r) => "fires "+r.fix)(_c) , 1); 
}, true);

function pushKeyToStroke(e) { /* :event -> :either string upkey */
  const nki = Stroke.upkey(e);
  if (!Stroke.modifierKeys.includes(nki.key)) {
    oneShotState.control.activateTimer();
    oneShotState.stroke.push(nki);
    return _right(nki);
  } else { return _left("skipped. key : "+ nki.code); }
}
function checkAndShrinkCandidates(e) { /* :event -> @[:either string record] */
  return () => {
    const prefixOf = Stroke.prefixOf(oneShotState.stroke);
    const shrinked = oneShotState.candidates.filter(
      (record) =>
        prefixOf(spy(record.stroke,3,Stroke.showStroke)) && record.predicate(e.target)
    );
    debugout("chk&shrnk.cs.1",3);
    const record = shrinked.find(
      (record) => Stroke.sameOf(oneShotState.stroke)(record.stroke)
    );
    debugout("chk&shrnk.cs.2",3);
    if (record === undefined) {
      if (shrinked.every( (r) => Stroke.isAnyPrefixStroke(r.stroke))) {
        oneShotState.control.resetStroke();
        oneShotState.control.killTimer();
      } else {
        oneShotState.candidates = shrinked;
      }
    } else {
      oneShotState.control.activateTimer();
      return _right(record);
    }
    return _left("no matched stroke");
  };
}
function fireRecord(e) { /* :event -> :record -> :either string record */
  return (record) => {
    oneShotState.control.resetStroke();
    oneShotState.control.killTimer();
    if (record.keymap == "text" && e.target.value === undefined) {
      return _left("text keymap function doesn't work target element has no property.value"); }
    e.preventDefault();
    if (!record.propagation){
      e.stopPropagation();
    }
    record.func(e.target);
    return _right(record);
  };
}


addShotBox();

