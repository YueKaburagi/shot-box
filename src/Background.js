/** eslint hint **/
/* global global:writable, exports:readonly */

"use strict";

exports.setGlobal = function (data) {
  return function() {
    global = data;
  };
};
exports.getGlobal = function () {
  return global;
};


exports.setBrowserActionIcon = function (details) {
  return function () {
    return browser.browserAction.setIcon(details);
  };
};




exports.fireTabsGet = function (ix) {
  return function () {
    return browser.tabs.get(ix);
  };
};
exports.fireTabsQuery = function (q) {
  return function () {
    return browser.tabs.query(q);
  };
};
exports.fireTabsRemove = function (ix) {
  return function () {
    return browser.tabs.remove(ix);
  };
};
exports.fireTabsUpdate = function (ix) {
  return function (q) {
    return function () {
      return browser.tabs.update(ix,q);
    };
  };
};
exports.fireTabsCreate = function (q) {
  return function () {
    return browser.tabs.create(q);
  };
};
