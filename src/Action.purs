module Action where

import Prelude

import Effect (Effect)
import Data.Maybe (Maybe(..))

import Action.Types
import Message (BgResult(..))
import Runtime.Port (Port)
import State (getGlobal)

import Log as Log

actionDoNothing :: SboxFunction
actionDoNothing =
  { fix : "action nothing"
  , func : \_ _ -> pure unit
  }



callbackAcceptSucceed :: String -> BgResult -> Effect Unit
callbackAcceptSucceed _ BRSucceed = pure unit
callbackAcceptSucceed s x = Log.withErrorShow s ("unexpected : " <> show x)
withPort :: String -> (Port -> Effect Unit) -> Effect Unit
withPort s f = do
  port <- _.connection <$> getGlobal
  case port of
    Just p -> f p
    Nothing -> Log.withErrorShow s "no connection"
