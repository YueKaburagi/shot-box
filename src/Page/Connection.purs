
module Page.Connection where

import Prelude

import Effect (Effect)
import Data.Maybe (Maybe(..))

import State (modifyGlobal,getGlobal)
import Message (BgMessage(..), BgAction(..), BgResult(..), TabInfo)
import Runtime.Port (Port,connectSimple,genOnMessageListener,addPortListenerOnMessage)

makeConnection :: Effect Unit
makeConnection = do
  port <- connectSimple
  modifyGlobal (_ { connection = Just port })
  hearL <- genOnMessageListener listenerImpl
  addPortListenerOnMessage hearL port

listenerImpl :: Port -> BgMessage -> Effect Unit
listenerImpl p (BgRes r) =
  case r of
    BRResultActive b -> -- BgReverseAction ??
      callbackSetActive b
    _ -> do
      cb <- _.callback <$> getGlobal
      cb r
listenerImpl _ _ = pure unit

callbackSetActive :: Boolean -> Effect Unit
callbackSetActive b =
  modifyGlobal (_ { active = b })

