
module Util.Timer where

import Prelude

import Effect (Effect)
import Effect.Timer (TimeoutId)
import Effect.Timer as Timer
import Data.Maybe (Maybe(..))


killTimer :: Maybe TimeoutId -> Effect Unit
killTimer tid =
  case tid of
    Just t -> Timer.clearTimeout t
    _ -> pure unit
