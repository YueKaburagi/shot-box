
module Log where

import Prelude
import Effect (Effect)
import Effect.Console (error, warn, log)


withErrorShow :: forall a . (Show a) => String -> a -> Effect Unit
withErrorShow prefix a = error ("[sb:" <> prefix <> "]" <> show a)

withWarnShow :: forall a . (Show a) => String -> a -> Effect Unit
withWarnShow prefix a = warn ("[sb:" <> prefix <> "]" <> show a)

withLogShow :: forall a . (Show a) => String -> a -> Effect Unit
withLogShow prefix a = log ("[sb:" <> prefix <> "]" <> show a)

withDebugShow :: forall a . (Show a) => String -> a -> Effect Unit
withDebugShow prefix a = log ("_[sb:" <> prefix <> "]" <> show a)
-- withDebugShow _ _ = pure unit
