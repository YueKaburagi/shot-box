module Action.Tab where

import Prelude

import Effect (Effect)
import Data.Maybe (Maybe(..), maybe)
import Data.Nullable (toMaybe)
import Data.Foldable (foldr)
import Data.String as String
import Data.Array as Array
import Data.Array.NonEmpty as NEA
import Data.String.Regex (test, match)
import Data.Tuple (Tuple(..))

import Web.HTML.Window (Window)
import Web.HTML.Window as Window
import Web.HTML.HTMLElement as Element
import Web.HTML.HTMLInputElement as Input
import Web.HTML.HTMLTextAreaElement as TextArea

import Action.Types (SboxFunction)
import Action (withPort, callbackAcceptSucceed)
import Action.Text (execCopy)
import Message (BgAction(..), BgResult(..))
import Runtime.Port (postAction)
import State (getGlobal,modifyGlobal)
import Shotbox.Shotbox (showShotbox,hideShotbox, showTextpane,hideTextpane)
import Shotbox.Indicator (showIndicator)
import Shotbox.Shotbox.Parser (Constraint(..), StrGenAtom(..))

import Log as Log



actionCloseCurrentTab :: SboxFunction
actionCloseCurrentTab =
  { fix : "tab close-current"
  , func : \_ _ -> do
    withPort "page-actionCloseCurrentTab" (
      \port -> do
        modifyGlobal (_ { callback = callbackAcceptSucceed "page-cb-actionCloseCurrentTab" })
        postAction port BACloseCurrentTab
      )
  }
semiActionShiftTabFocus :: Int -> SboxFunction
semiActionShiftTabFocus n =
  let
    here = "page-actionShiftTabFocus " <> show n
  in
  { fix : "tab shift focus"
  , func : \_ _ -> do
    withPort here (
      \port -> do
        modifyGlobal (_ { callback = callbackAcceptSucceed here })
        postAction port (BAShiftTabFocus n)
      )
  }

actionCopyUrl :: SboxFunction
actionCopyUrl =
  { fix : "tab copy-url"
  , func : \w _ -> do
    withPort "page-actionCopyUrl" (
      \port -> do
        modifyGlobal (_ { callback = callbackActionCopyUrl w })
        postAction port BAGetCurrentTabInfo
        Log.withDebugShow "page-actionCopyUrl" "fire"
      )
  }
callbackActionCopyUrl :: Window -> BgResult -> Effect Unit
callbackActionCopyUrl w (BRResultGetCurrentTabInfo info) =
  case toMaybe info.url of
    Just u -> do
      element <- _.shotboxInstance <$> getGlobal
      case element of
        Just e -> do
          showShotbox u
          dir <- Input.selectionDirection e
          Input.setSelectionRange 0 (String.length u) dir e
          execCopy =<< Window.document w
          Log.withDebugShow "page-cb-actionCopyUrl" "execCopy"
          showIndicator ("[copy] \"" <> u <> "\"")
          hideShotbox
        Nothing -> Log.withErrorShow "page-cb-actionCopyUrl" "no shotbox instance"
    Nothing -> Log.withErrorShow "page-cb-actionCopyUrl" "no info url"
callbackActionCopyUrl _ _ = Log.withErrorShow "page-cb-actionCopyUrl" "unexpected Result type"


actionCopyTitle :: SboxFunction
actionCopyTitle =
  { fix : "tab copy-title"
  , func : \w _ -> do
    withPort "page-actionCopyTitle" (
      \port -> do
        modifyGlobal (_ { callback = callbackActionCopyTitle w })
        postAction port BAGetCurrentTabInfo
        Log.withDebugShow "page-actionCopyTitle" "fire"
      )
  }
callbackActionCopyTitle :: Window -> BgResult -> Effect Unit
callbackActionCopyTitle w (BRResultGetCurrentTabInfo info) =
  case toMaybe info.title of
    Just t -> do
      doCopy "page-cb-actionCopyTitle" w t
    Nothing -> Log.withErrorShow "page-cb-actionCopyTitle" "no title info"
callbackActionCopyTitle _ _ = Log.withErrorShow "page-cb-actionCopyTitle" "unexpected Result type"



actionStrGen :: SboxFunction
actionStrGen =
  { fix : "tab generate-string"
  , func : \w e -> do
    withPort "page-actionStrGen" (
      \port -> do
        modifyGlobal (_ { callback = callbackActionStrGen "mk-bu-record" w })
        postAction port BAGetCurrentTabInfo
        Log.withDebugShow "page-actionStrGen" "fire"
      )
  }
callbackActionStrGen :: String -> Window -> BgResult -> Effect Unit
callbackActionStrGen index w (BRResultGetCurrentTabInfo info) =
  case Tuple (toMaybe info.title) (toMaybe info.url) of
    Tuple (Just t) (Just u) -> do
      rules <- (Array.find (\s -> s.index == index) <<< _.strGenMap) <$> getGlobal
      case rules of
        Just s ->
          case Array.find (\r -> test r.matcher u) s.records of
            Just r -> do
              ss <- pure $ (maybe "" identity <<< generateS t u) <$> r.generator
              doCopy "page-cb-actionStrGen" w (foldr (<>) "" ss)
            Nothing -> do
              Log.withWarnShow "page-cb-actionStrGen" "title pattern unmatched"
        Nothing -> Log.withWarnShow "page-cb-actionStrGen" "no rule"
    Tuple _ _ -> Log.withErrorShow "page-cb-actionStrGen" "no info url/title"
callbackActionStrGen _ _ _ =
  Log.withErrorShow "page-cb-actionStrGen" "unexpected Result type"

generateS :: String -> String -> StrGenAtom -> Maybe String
generateS _ url (Url (ReStrHot _ re)) = ((join <<< flip NEA.index 1) =<< match re url)
generateS _ url (Url _) = pure url
generateS title _ (Title (ReStrHot _ re)) = ((join <<< flip NEA.index 1) =<< match re title)
generateS title _ (Title _) = pure title 
generateS _ _ (Text s) = pure s


doCopy :: String -> Window -> String -> Effect Unit
doCopy place w str = do
  element <- _.textpaneInstance <$> getGlobal
  case element of
    Just e -> do
      Element.focus (TextArea.toHTMLElement e)
      showTextpane str
      TextArea.select e
      execCopy =<< Window.document w
      showIndicator ("[copy] \"" <> str <> "\"")
      hideTextpane
    Nothing -> Log.withErrorShow place "no textpane instance"

