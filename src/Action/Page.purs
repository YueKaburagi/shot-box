module Action.Page where


import Prelude

import Effect (Effect)
import Data.Int (round, toNumber, ceil)

import Web.HTML.Window (Window)
import Web.HTML.Window as Window
import Web.HTML.HTMLElement (HTMLElement)
import Web.HTML.HTMLElement as Element

import Web.DOM.Element as DOM

import Action.Types (SboxFunction)


semiActionScrollPage :: Number -> Window -> HTMLElement -> Effect Unit
semiActionScrollPage r w _ = do
  h <- ((*) r <<< toNumber) <$> Window.innerHeight w
  Window.scrollBy 0 (round h) w

actionNextPage :: SboxFunction
actionNextPage =
  { fix : "view next-page"
  , func : semiActionScrollPage 0.90
  }
actionPrevPage :: SboxFunction
actionPrevPage =
  { fix : "view prev-page"
  , func : semiActionScrollPage (-0.90)
  }
actionNextLine :: SboxFunction
actionNextLine =
  { fix : "view next-line"
  , func : semiActionScrollPage 0.08
  }
actionPrevLine :: SboxFunction
actionPrevLine =
  { fix : "view prev-line"
  , func : semiActionScrollPage (-0.08)
  }

semiActionScroll :: Int -> Window -> HTMLElement -> Effect Unit
semiActionScroll n w _ =
  Window.scroll 0 n w

actionScrollToTop :: SboxFunction
actionScrollToTop =
  { fix : "view beginning-of-page"
  , func : semiActionScroll 0
  }

actionScrollToBottom :: SboxFunction
actionScrollToBottom =
  { fix : "view end-of-page"
  , func : \window element -> do
    sH <- DOM.scrollHeight (Element.toElement element)
    cH <- DOM.clientHeight (Element.toElement element)
    semiActionScroll (ceil (sH - cH)) window element
  }

