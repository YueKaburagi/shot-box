module Action.Text ( emulateBackwardChar
                   , emulateBackwardCharSel
                   , emulateDeleteBackwardChar
                   , emulateDeleteForwardChar
                   , emulateForwardChar
                   , emulateForwardCharSel
                   , emulateKillLine
                   , emulateKillRingSave
                   , emulateMoveBeginningOfLine
                   , emulateMoveEndOfLine
                   , actionKeyboardQuit
                   , actionInsertNewline
                   -- helps copy to clipboard
                   , execCopy
                   )  where

import Prelude

import Effect (Effect)
import Effect.Promise (Promise)
import Data.Maybe (Maybe, maybe)
import Data.String (Pattern(..))
import Data.String as String
import Control.Alt ((<|>))

import Web.HTML.Window (Window)
import Web.HTML.Window as Window
import Web.HTML.Navigator (Navigator)
import Web.HTML.HTMLDocument (HTMLDocument)
import Web.HTML.HTMLElement (HTMLElement)
import Web.HTML.HTMLElement as Element
import Web.HTML.HTMLInputElement (HTMLInputElement)
import Web.HTML.HTMLInputElement as Input
import Web.HTML.HTMLTextAreaElement (HTMLTextAreaElement)
import Web.HTML.HTMLTextAreaElement as TextArea

import Action.Types (SboxFunction)

class InputLike e where
  selectionDirection :: e -> Effect String
  selectionEnd :: e -> Effect Int
  selectionStart :: e -> Effect Int
  setSelectionRange :: Int -> Int -> String -> e -> Effect Unit
  value :: e -> Effect String
  setValue :: String -> e -> Effect Unit
  fromHTMLElement :: HTMLElement -> Maybe e
  -- fromElement :: Element -> Maybe e
  -- fromNode :: Node -> Maybe e
  -- fromChildNode :: ChildNode -> Maybe e
  -- fromNonDocumentTypeChildNode :: NonDocumentTypeChildNode -> Maybe e
  -- fromParentNode :: ParentNode -> Maybe e
  -- fromEventTarget :: EventTarget -> Maybe e
  toHTMLElement :: e -> HTMLElement
  -- toElement :: e -> Element
  -- others

instance inputLikeInput :: InputLike HTMLInputElement where
  selectionDirection = Input.selectionDirection
  selectionStart = Input.selectionStart
  selectionEnd = Input.selectionEnd
  setSelectionRange = Input.setSelectionRange
  value = Input.value
  setValue = Input.setValue
  fromHTMLElement = Input.fromHTMLElement
  toHTMLElement = Input.toHTMLElement

instance inputLikeTextArea :: InputLike HTMLTextAreaElement where
  selectionDirection = TextArea.selectionDirection
  selectionStart = TextArea.selectionStart
  selectionEnd = TextArea.selectionEnd
  setSelectionRange = TextArea.setSelectionRange
  value = TextArea.value
  setValue = TextArea.setValue
  fromHTMLElement = TextArea.fromHTMLElement
  toHTMLElement = TextArea.toHTMLElement



getHeadCaretPos :: forall e . InputLike e => e -> Effect Int
getHeadCaretPos e = do
  dir <- selectionDirection e
  if dir == "forward" then selectionEnd e else selectionStart e

getLastCaretPos :: forall e . InputLike e => e -> Effect Int
getLastCaretPos e = do
  dir <- selectionDirection e
  if dir == "backward" then selectionStart e else selectionEnd e

getXxxOfLinePos :: forall e . InputLike e
                => (Int -> Int -> Int -> String -> Int) -> e -> Effect Int
getXxxOfLinePos f e = do
  p0 <- getHeadCaretPos e
  tx <- value e
  pS <- pure 0
  pE <- pure (String.length tx)
  p <- pure (f pS pE p0 tx)
  pure (max pS (min p pE))

getBeginningOfLinePos :: forall e . InputLike e => e -> Effect Int
getBeginningOfLinePos =
  getXxxOfLinePos (\pS _ p0 tx -> maybe pS ((+) 1) (String.lastIndexOf' (Pattern "\n") p0 tx))

getEndOfLinePos :: forall e . InputLike e => e -> Effect Int
getEndOfLinePos =
  getXxxOfLinePos (\_ pE p0 tx -> maybe pE ((-) 1) (String.indexOf' (Pattern "\n") p0 tx))

emulateMoveBeginningOfLine :: SboxFunction
emulateMoveBeginningOfLine =
  { fix : "emulate move-beginning-of-line"
  , func : \_ element -> do
    maybe (pure unit) identity (
      (fn <$> Input.fromHTMLElement element) <|>
      (fn <$> TextArea.fromHTMLElement element)
      )
  }
  where
    fn :: forall e . InputLike e => e -> Effect Unit
    fn e = do
      pS <- getBeginningOfLinePos e
      dir <- selectionDirection e
      setSelectionRange pS pS dir e
      -- to fix with Shift

emulateMoveEndOfLine :: SboxFunction
emulateMoveEndOfLine =
  { fix : "emulate move-end-of-line"
  , func : \_ element -> do
    maybe (pure unit) identity (
      (fn <$> Input.fromHTMLElement element) <|>
      (fn <$> TextArea.fromHTMLElement element)
      )
  }
  where
    fn :: forall e . InputLike e => e -> Effect Unit
    fn e = do
        pE <- getEndOfLinePos e
        dir <- selectionDirection e
        setSelectionRange pE pE dir e


semiEmulateXxxwardChar :: forall e . InputLike e
                       => (Int -> Int)
                       -> (e -> Effect (Int -> Int))
                       -> Boolean -> e -> Effect Unit
semiEmulateXxxwardChar succ _bound mode e = do
  bound <- _bound e
  d <- selectionDirection e
  case mode of
    true -> do
      pE <- (if d == "forward" then (bound <<< succ) else identity) <$> getLastCaretPos e
      pS <- (if d /= "forward" then (bound <<< succ) else identity) <$> getHeadCaretPos e
      if (pS > pE)
        then setSelectionRange pE pS (if d == "forward" then "backward" else d) e
        else setSelectionRange pS pE d e
    false -> do
      pN <- (bound <<< succ) <$> (if d == "forward" then getLastCaretPos e else getHeadCaretPos e)
      setSelectionRange pN pN d e
          

semiEmulateBackwardChar :: Boolean -> HTMLElement -> Effect Unit
semiEmulateBackwardChar b element = do
  maybe (pure unit) identity (
    (fn <$> Input.fromHTMLElement element) <|>
    (fn <$> TextArea.fromHTMLElement element)
    )
  where
    fn :: forall e . InputLike e => e -> Effect Unit
    fn = semiEmulateXxxwardChar ((+) (-1)) (const ( pure (max 0))) b

semiEmulateForwardChar :: Boolean -> HTMLElement -> Effect Unit
semiEmulateForwardChar b element = do
  maybe (pure unit) identity (
    (fn <$> Input.fromHTMLElement element) <|>
    (fn <$> TextArea.fromHTMLElement element)
    )
  where
    fn :: forall e . InputLike e => e -> Effect Unit
    fn = semiEmulateXxxwardChar ((+) 1) (\e -> (min <<< String.length) <$> value e) b

emulateBackwardChar :: SboxFunction
emulateBackwardChar =
  { fix : "emulate backward-char"
  , func : const (semiEmulateBackwardChar false)
  }
emulateBackwardCharSel :: SboxFunction
emulateBackwardCharSel =
  { fix : "emulate backward-char-select"
  , func : const (semiEmulateBackwardChar true)
  }
emulateForwardChar :: SboxFunction
emulateForwardChar =
  { fix : "emulate forward-char"
  , func : const (semiEmulateForwardChar false)
  }
emulateForwardCharSel :: SboxFunction
emulateForwardCharSel =
  { fix : "emulate forward-char-select"
  , func : const (semiEmulateForwardChar true)
  }


emulateDeleteBackwardChar :: SboxFunction
emulateDeleteBackwardChar =
  { fix : "emulate delete-backward-char"
  , func : \_ element -> do
  maybe (pure unit) identity (
    (fn <$> Input.fromHTMLElement element) <|>
    (fn <$> TextArea.fromHTMLElement element)
    )
  }
  where
    fn :: forall e . InputLike e => e -> Effect Unit
    fn = semiEmulateDeleteXxxwardChar (
    \e -> do
      pS <- getHeadCaretPos e
      pE <- getLastCaretPos e
      pN <- pure (if pS == pE then max 0 (pS - 1) else pS)
      pure { s : pN, e : pE, c : pN }
    )
emulateDeleteForwardChar :: SboxFunction
emulateDeleteForwardChar =
  { fix : "emulate delete-forward-char"
  , func : \_ element -> do
  maybe (pure unit) identity (
    (fn <$> Input.fromHTMLElement element) <|>
    (fn <$> TextArea.fromHTMLElement element)
    )
  }
  where
    fn :: forall e . InputLike e => e -> Effect Unit
    fn = semiEmulateDeleteXxxwardChar (
    \e -> do
      pS <- getHeadCaretPos e
      pE <- getLastCaretPos e
      pM <- String.length <$> value e
      pN <- pure (if pS == pE then min pM (pS + 1) else pE)
      pure { s : pS, e : pN, c : pS }
    )

semiEmulateDeleteXxxwardChar :: forall e . InputLike e
                             => (e -> Effect {s :: Int, e :: Int, c :: Int})
                             -> e -> Effect Unit
semiEmulateDeleteXxxwardChar fP e = do
  p <- fP e
  tx <- value e
  setValue (String.take p.s tx <> String.drop p.e tx) e
  d <- selectionDirection e
  setSelectionRange p.c p.c d e


actionInsertNewline :: SboxFunction
actionInsertNewline =
  { fix : "emulate insert-newline"
  , func : \_ element -> do
    case TextArea.fromHTMLElement element of
      Just e -> do
        pS <- getHeadCaretPos e
        pE <- getLastCaretPos e
        tx <- value e
        setValue (String.take pS tx <> "\n" <> String.drop pE tx) e
        d <- selectionDirection e
        setSelectionRange pS pS d e
      Nothing -> pure unit
  }

-- navigate.clipboard works https only. uncomfortable function
foreign import writeText :: Navigator -> String -> Effect (Promise Unit)

foreign import execCopy :: HTMLDocument -> Effect Unit
foreign import execCut :: HTMLDocument -> Effect Unit

emulateKillRingSave :: SboxFunction
emulateKillRingSave =
  { fix : "emulate kill-ring-save"
  , func : \window element -> do
    maybe (
      do
        doc <- Window.document window
        execCopy doc
      ) identity (
      (fn window <$> Input.fromHTMLElement element) <|>
      (fn window <$> TextArea.fromHTMLElement element)
      )
  }
  where
    fn :: forall e . InputLike e => Window -> e -> Effect Unit
    fn window e = do
      doc <- Window.document window
      execCopy doc
      d <- selectionDirection e
      pN <- if d == "forward" then getLastCaretPos e else getHeadCaretPos e
      setSelectionRange pN pN d e

emulateKillLine :: SboxFunction
emulateKillLine =
  { fix : "emulate kill-line"
  , func : \window element -> do
    maybe (pure unit) identity (
      (fn window <$> Input.fromHTMLElement element) <|>
      (fn window <$> TextArea.fromHTMLElement element)
      )
  }
  where
    fn :: forall e . InputLike e => Window -> e -> Effect Unit
    fn window e = do
      pS <- getHeadCaretPos e
      pE <- getEndOfLinePos e
      setSelectionRange pS pE "forward" e
      doc <- Window.document window
      execCut doc
      setSelectionRange pS pS "forward" e

actionKeyboardQuit :: SboxFunction
actionKeyboardQuit =
  { fix : "action keyboard-quit"
  , func : \_ element -> do
    Element.blur element
  }
