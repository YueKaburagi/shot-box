
module Action.Types where

import Prelude

import Effect (Effect)
import Web.HTML.Window (Window)
import Web.HTML.HTMLElement (HTMLElement)


type SboxFunction = { fix :: String, func :: Window -> HTMLElement -> Effect Unit }
type SboxTransform = { fix :: String, func :: String -> Effect String }
-- Window -> Element -> Effect String
-- SboxChaining = { fix :: String, boxes : Sbox }
-- Sbox
