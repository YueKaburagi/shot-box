
module Action.Shotbox where

import Prelude

import Effect (Effect)
import Effect.Promise (Promise)
import Data.Maybe (Maybe(..), maybe)
import Data.String (Pattern(..))
import Data.String as String
import Data.Array as Array

import Web.HTML.Window (Window)
import Web.HTML.Window as Window
import Web.HTML.Navigator (Navigator)
import Web.HTML.HTMLDocument (HTMLDocument)
import Web.HTML.HTMLElement (HTMLElement)
import Web.HTML.HTMLElement as Element
import Web.HTML.HTMLInputElement (HTMLInputElement)
import Web.HTML.HTMLInputElement as Input

import Action.Types (SboxFunction)
import Shotbox.Shotbox (showShotbox)


actionShowNavigationShotbox :: SboxFunction
actionShowNavigationShotbox =
  { fix : "shotbox show-navigation-shotbox"
  , func : \_ _ -> do
    showShotbox ""
  }


