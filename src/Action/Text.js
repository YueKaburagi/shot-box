/** eslint hint **/
/* global exports */

"use strict";

exports.execCopy = function (doc) {
  return function () {
    return doc.execCommand('copy');
  };
};

exports.execCut = function (doc) {
  return function () {
    return doc.execCommand('cut');
  };
};

exports.writeText = function (nav) {
  return function(str) {
    return function() {
      return nav.clipboard.writeText(str);
    };
  };
};
