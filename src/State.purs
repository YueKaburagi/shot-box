
module State where

import Prelude

import Effect (Effect)
import Effect.Timer (TimeoutId)
import Data.Maybe (Maybe(..))

import Web.HTML.HTMLSpanElement (HTMLSpanElement)
import Web.HTML.HTMLInputElement (HTMLInputElement)
import Web.HTML.HTMLTextAreaElement (HTMLTextAreaElement)

import KeyStroke (ActRecord, Stroke)
import Runtime.Port (Port)
import Message (BgResult)
import Configure (StrGenMapHot)


type ShotBoxState =
  { records :: Array ActRecord
  , stroke :: Stroke
  , strokeTimer :: Maybe TimeoutId
  , shotboxInstance :: Maybe HTMLInputElement
  , indicatorInstance :: Maybe HTMLSpanElement
  , indicatorTimer :: Maybe TimeoutId
  , textpaneInstance :: Maybe HTMLTextAreaElement
  , connection :: Maybe Port
  , callback :: BgResult -> Effect Unit
  , active :: Boolean
  , strGenMap :: StrGenMapHot
  }
initState :: ShotBoxState
initState =
  { records : []
  , stroke : []
  , strokeTimer : Nothing
  , shotboxInstance : Nothing
  , indicatorInstance : Nothing
  , indicatorTimer : Nothing
  , textpaneInstance : Nothing
  , connection : Nothing
  , callback : const (pure unit)
  , active : true
  , strGenMap : []
  }

-- workaround
foreign import setGlobal :: ShotBoxState -> Effect Unit
foreign import getGlobal :: Effect ShotBoxState
modifyGlobal :: (ShotBoxState -> ShotBoxState) -> Effect Unit
modifyGlobal f = do
  setGlobal =<< (f <$> getGlobal)
