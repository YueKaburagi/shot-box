

const action = {internal: {}, debug: {}};


// for RichTextEditor
function emulateKeyInput() {

}
action.emulateKeyInput = emulateKeyInput;


action.internal.getLeadCaretPos = function(element) {
  return (getSelectionDirection(element) == "forward") ? element.selectionEnd : element.selectionStart;
};
action.internal.getInvertedLeadCaretPos = function(element) {
  return (getSelectionDirection(element) == "backward") ? element.selectionStart : element.selectionEnd;
};
function getSelectionDirection(element) {
  let d = element.selectionDirection;
  if (d == "none") {
    d = oneShotConfig.nonDirectionalSelectionEnds;
  }
  return d;
}
action.internal.getSelectionDirection = getSelectionDirection;
// should use window.getSelection() : Selection
//  Selection contains multi selection

action.internal.getBeginningOfLinePos = function(element){
  let p = action.internal.getInvertedLeadCaretPos(element);
  const pS = 0;
  const tx = element.value;
  while (p > pS &&
         "\r\n" != tx.substring(Math.max(0,p-2), p) &&
         "\n" != tx.charAt(Math.max(0,p-1))
        ) { p -= 1; }
  return p;
};
action.internal.getEndOfLinePos = function(element){
  let p = action.internal.getLeadCaretPos(element);
  const pE = element.value.length;
  const tx = element.value;
  while (p < pE && "\r\n" != tx.substring(p, p+2) && "\n" != tx.charAt(p) ) { p += 1; }
  return p;
};

function emulateXxxwardChar(succ,_bound) {
  return (modeSelection) => {
    return (element) => {
      const bound = _bound(element);
      let pS = element.selectionStart;
      let pE = element.selectionEnd;
      const d = action.internal.getSelectionDirection(element);
      if (modeSelection) {

        if (d == "forward") {
          pE = bound(succ(pE));
        } else {
          pS = bound(succ(pS));
        }

        if (pS > pE) {
          element.setSelectionRange(pE,pS, d == "forward" ? "backward" : "forward");
        } else {
          element.setSelectionRange(pS,pE, d);
        }
        
      } else {
        const pN = bound(succ( d == "forward" ? pE : pS ));
        element.setSelectionRange(pN,pN);
      }
    };
  };
}
action.emulateBackwardChar = // C-b
      emulateXxxwardChar((x) => x - 1,
                          (_) => {
                            return (x) => Math.max(0, x);
                          });
action.emulateForwardChar = // C-f
      emulateXxxwardChar((x) => x + 1,
                         (e) => {
                           const M = e.value.length;
                           return (x) => Math.min(M, x);
                         });
action.emulateMoveBeginningOfLine = function(element){ // C-a
  const pS = action.internal.getBeginningOfLinePos(element);
  element.setSelectionRange(pS,pS);
};
action.emulateMoveEndOfLine = function(element){ // C-e
  const pE = action.internal.getEndOfLinePos(element);
  element.setSelectionRange(pE,pE);
};



action.emulateDeleteBackwardChar = function(element) { // C-h
  const pS = element.selectionStart;
  const pE = element.selectionEnd;
  let pN = null;
  const ostr = element.value;

  let str = null;
  if (pS == pE) {
    pN = Math.max(pS -1, 0);
    str = ostr.slice(0, pN) + ostr.slice(pS, ostr.length);
  } else {
    pN = pS;
    str = ostr.slice(0, pS) + ostr.slice(pE, ostr.length);
  }
  
  element.value = str;
  element.setSelectionRange(pN,pN);
};
action.emulateDeleteForwardChar = function(element) { // <delete> maybe C-d too
  const pS = element.selectionStart;
  const pE = element.selectionEnd;
  const ostr = element.value;

  let str = null;
  if (pS == pE) {
    const pC = Math.min(pS +1, ostr.length);
    str = ostr.slice(0, pS) + ostr.slice(pC, ostr.length);
  } else {
    str = ostr.slice(0, pS) + ostr.slice(pE, ostr.length);
  }
  
  element.value = str;
  element.setSelectionRange(pS,pS);
};
action.emulateKillRingSave = function(element) { // M-w
  document.execCommand('copy');
  const d = action.internal.getSelectionDirection(element);
  const pN = (d == "forward") ? element.selectionEnd : element.selectionStart;
  element.setSelectionRange(pN,pN); 
};
action.emulateKillLine = function(element) { // C-k
  // support INPUT only
  const pS = action.internal.getLeadCaretPos(element);
  const pE = action.internal.getEndOfLinePos(element);
  element.setSelectionRange(pS,pE);
  document.execCommand('cut');
  element.setSelectionRange(pS,pS);
};
action.emulateYank = function(_) { // C-y  // forbidden. 
  document.execCommand('paste');
};
function keyboardQuit(_) { // C-g
  window.blur(); // remove focus
}
action.keyboardQuit = keyboardQuit;
function doNothing(_) {}
action.doNothing = function(_){};

action.scrollPage = function(r){ // C-v,C-z,C-p,C-n
  if (0.01 <= Math.abs(r) && Math.abs(r) < 100.0) {
    return (_) => {
      let h = window.innerHeight * r;
      window.scrollBy({ top: h, left: 0, behavior: "smooth" });
    };
  } else (_) => {};
};
function scrollTo(x) {
  return (_) => {
    window.scrollTo({ top: x, behavior: "smooth" });
  }
}
const scrollToTop = scrollTo(0);
const scrollToBottom = // window.scrollMaxY supports only firefox
      scrollTo(window.scrollMaxY + window.innerHeight);
// 後から挿入された要素の高さがわからない？

function tabCloseCurrent(_) {
  shotBoxConnection.control.postMessage({msg: "close current tab"});
}
action.tabCloseCurrent = tabCloseCurrent;
action.tabNext = function(_){
  shotBoxConnection.control.postMessage({msg: "focus tab", args: {offset: 1}});
};
action.tabPrev = function(_){
  shotBoxConnection.control.postMessage({msg: "focus tab", args: {offset: -1}});
};
action.copyUrl = function(_) {
  shotBoxConnection.control.pushCallback("getCurrentTabInfo")(
    (data) => {
      oneShotState.showShotBox(data.url);
      oneShotState.shotbox.setSelectionRange(0, data.url.length);
      document.execCommand('copy');
      oneShotState.showIndicator('[copy] "'+data.url+'"');
      oneShotState.hideShotBox();
    });
  shotBoxConnection.control.postMessage({msg: "get current tab info"});
};
action.blankShotBox = function(_) {
  oneShotState.showShotBox("");
};

action.copyTitleWithFix = function(_){
  const fixRules = [{
    tagsSCR: "//*[contains(@class, 'TagContainer-area')]//*[contains(@class, 'TagItem-name')]/text()",
    urlRE: new RegExp("^https?://.+\\.nicovideo\\.jp/watch"),
    url: function(s){return this.urlRE.test(s); },
    titleRE: new RegExp("^(.+) - ニコニコ動画.*$"),
    title: function(s){return this.titleRE[Symbol.match](s)[1]; }
  },{
    urlRE: new RegExp("^https?://.+\\.nicovideo\\.jp/mylist"),
    url: function(s){return this.urlRE.test(s); },
    titleRE: new RegExp("^「(.+)」.* さんの公開マイリスト.*$"),
    title: function(s){return this.titleRE[Symbol.match](s)[1]; }
  },{
    urlRE: new RegExp("^https?://.+\\.nicovideo\\.jp/user/.+/video"),
    url: function(s){return this.urlRE.test(s); },
    titleRE: new RegExp("^(.+) - niconico.*$"),
    title: function(s){return this.titleRE[Symbol.match](s)[1]; }
  },{
    urlRE: new RegExp("^https?://seiga\\.nicovideo\\.jp/user/illust"),
    url: function(s){return this.urlRE.test(s); },
    titleRE: new RegExp("^(.+) さんのイラスト一覧.*$"),
    title: function(s){return this.titleRE[Symbol.match](s)[1]; }
  },{
    urlRE: new RegExp("^https?://.+\\.pixiv\\.net/member\\.php\\?id="),
    url: function(s){return this.urlRE.test(s); },
    titleRE: new RegExp("^(.+) - pixiv.*$"),
    //titleRE: new RegExp("^「(.+)」のプロフィール.*$"),
    title: function(s){return this.titleRE[Symbol.match](s)[1]; }
  },{
    tagsSCR: "//*[contains(@class, 'ijhsb.eoyYjA')]//*[contains(@class, '_3Xr7iJv')]/text()",
    //  難読化されてるけど、ページ毎に違うとかはなさそう？
    urlRE: new RegExp("^https?://.+\\.pixiv\\.net/member_illust.php\\?mode=medium"),
    url: function(s){return this.urlRE.test(s); },
    titleRE: new RegExp("^.*(.+) - .+の.*$"),
    //titleRE: new RegExp("^.*「(.+)」/「.+」の.*$"),
    title: function(s){return this.titleRE[Symbol.match](s)[1]; }
  },{
    tagsSCR: "//*[contains(@class, 'illust_tag')]//li[contains(@class, 'tag')]/a/text()",
    urlRE: new RegExp("^https?://seiga\\.nicovideo\\.jp/seiga"),
    url: function(s){return this.urlRE.test(s); },
    titleRE: new RegExp("^(.+) / .+ さんのイラスト.*$"),
    title: function(s){return this.titleRE[Symbol.match](s)[1]; }
  }];
  const orElse = commonFunctions.semi.orElse;
  shotBoxConnection.control.pushCallback("getCurrentTabInfo")(
    (data) => {
      const url = data.url;
      const q = orElse(() => {return {title: commonFunctions.strict.id};})(
        fixRules.find((p) => p.url(url)));
      const title = q.title(data.title);
      oneShotState.showShotBox(title);
      oneShotState.shotbox.setSelectionRange(0, title.length);
      document.execCommand('copy');
      oneShotState.showIndicator('[copy] "'+title+'"');
      oneShotState.hideShotBox();
    });
  shotBoxConnection.control.postMessage({msg: "get current tab info"});
};

action.copyUXR = function(_) {
  shotBoxConnection.control.pushCallback("getCurrentTabInfo")(
    (data) => {
      let WORK_IN_PROGRESS;
      const url = data.url;
      if (url.startsWith("https://www.nicovideo.jp/watch")) {
        const iter = document.evaluate("//*[contains(@class, 'TagContainer-area')]//*[contains(@class, 'TagItem-name')]/text()", document, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
        let node = iter.iterateNext();
        let mass = [];
        while (node != null) {
          //          mass.push(node.innerText);
          mass.push(node.textContent);
          node = iter.iterateNext();
        }
        oneShotState.showIndicator('[uxr] '+mass);
      }
    });
  shotBoxConnection.control.postMessage({msg: "get current tab info"});  
};

// view control one next/prev page par N (a harf of page, a third of page)
// view control accel next/prev pixel par N accel A with P to L (accel while/deccel unless key holding)
//   this accelarations share scrolling speed
//   note: key-repeat is NOT completely work every environments
//         generally: keydown -> keypress -> keydown -> keypress -> ... -> keyup
//         rarely: keydown -> keypress -> keyup -> keydown -> keypress -> keyup -> ...
// view control smart scroll
//   scroll to next <section> or <h*> (min: 10% viewport, max: 500%? viewport)
//   or user defined pattern
//     e.g. nicovideo.jp/watch/* div.class = "Grid TagContainer"
// toggle mute-current-tab
// copy current title with url
// copy current title with url (formatted)
// shift add-bookmark-mode
//   bookmark-mode
//     calcurate preffered tags from site-url
//     with local save
// shift console-mode (launch C-l)
//   type SPACE affirm to next word or isolation
//   search on web (setmode :s or s:<search-engine-keyword>)
//   lookup text from current tab (setmode :l)
//   find such title or url from tab (setmode :t)
//   find such title or url from bookmark (setmode :b)
//   detect bookmark has such tag (t:<tagname>) or invert (-t:<tagname>)
//   setmode escape (::<\S+>)
//   collection command (c:<command>)
//   regexp (r:<expr>)
//   move all matchs tab to current after/before (c:move-rightside/move-leftside)
// shift keymap-mode
// shift file-browser-mode (image only?) (lazy load)
//   paste as original url (original-url: {preffered: url, knowns: [url]})
//   paste as local file
//   paste as local file through img comppresser-API
// shift search-mode
// emulate yank
// emulate transpose-chars C-t
// shift direct-mode
//   for reader, for game, ...etc
// open tab
// shift HoK-mode
//   open here, open in new tab, open in new window, copy link url, save link url
//   target <a><img>

// bookmark appendix infomations

const futabaShowLast = (num) => (_) => {
  const vs = document.getElementsByTagName("table")
  const M = Math.max(0, vs.length - num);
  for (let i=0; i<M; i++) {
    if (vs[i].hasAttribute("border")) {
      vs[i].setAttribute("style", "display: none;");
    }
  }
};


function emulateNothing() { console.warn("fires emulate nothing function");}
const functionsDict = [
  {fix: "emulate delete-backward-char", func: action.emulateDeleteBackwardChar},
  {fix: "emulate delete-forward-char", func: action.emulateDeleteForwardChar},
  {fix: "emulate backward-char", func: action.emulateBackwardChar(false)},
  {fix: "emulate backward-char with selection", func: action.emulateBackwardChar(true)},
  {fix: "emulate forward-char", func: action.emulateForwardChar(false)},
  {fix: "emulate forward-char with selection", func: action.emulateForwardChar(true)},
  {fix: "emulate move-beginning-of-line", func: action.emulateMoveBeginningOfLine},
  {fix: "emulate move-end-of-line", func: action.emulateMoveEndOfLine},
  {fix: "emulate kill-ring-save", func: action.emulateKillRingSave},
  {fix: "emulate kill-line", func: action.emulateKillLine},
  {fix: "emulate yank", func: action.emulateYank},
  {fix: "view beginning-of-page", func: scrollToTop},
  {fix: "view end-of-page", func: scrollToBottom},
  {fix: "view next-page", func: action.scrollPage(0.90)},
  {fix: "view prev-page", func: action.scrollPage(-0.90)},
  {fix: "view next-line", func: action.scrollPage(0.08)},
  {fix: "view prev-line", func: action.scrollPage(-0.08)},
  {fix: "tab close-current", func: action.tabCloseCurrent},
  {fix: "tab focus-next", func: action.tabNext},
  {fix: "tab focus-prev", func: action.tabPrev},
  {fix: "copy current-tab-url", func: action.copyUrl},
  {fix: "copy current-tab-title-with-fix", func: action.copyTitleWithFix},
  {fix: "shotbox blank", func: action.blankShotBox},
  {fix: "futaba last50", func: futabaShowLast(50)},
  {fix: "do nothing", func: action.doNothing},
  {fix: "test uxr", func: action.copyUXR},
  {fix: "command quit", func: action.keyboardQuit}
];



