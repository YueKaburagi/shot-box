


// * { margin: none; padding: none; border: none; }
// open close css transition? 
// #shotBoxInput height: 26px;
// #shotBoxList height: 480px;
// .hidden = {display: none;}
// li = {list-style-type; none;}

// shotbox record
//  -commands : <command-name> [binded-stroke]
//            : tab close current [C-x k]
//  -search   : <suggested-search-word:if such service supported>...
//            :
//  -list *   : 'search' 'command' ...
//            : show these list
//  -help     : show usage



// <form target="_blank or _top" method=get action=uri><input name=q /></form>
// ... negative ...
// not only use for search
// -
// can change dynamically?

// shotbox works only _top


// ~/scrap/text.thml

// prefix-key [search]
// e.g.
// g [search] :: google
// : [search] :: use-default
// [search] :: use-default

function fireShotbox(str) {

  // want to use pegjs
  const re = /\s+/g;
  const args = str.split(re);
  const mode = undefinedMaybeS({type: "blank"}, (head) => {
      return {type: "search", key: head, query: args.slice(1)};
//    if (head.startsWith(':')) {
//      return {type: "search", key: head.slice(1), query: args.slice(1)};
//    } else {
//      return {type: "search", query: args};
//    }
  })(args[0]);

  if ( mode.type == "search" ) {
    const defaultSearchEngineUrl = "https://duckduckgo.com/?q=";
    const defaultSearchEngine = {key:undefined, url:"https://duckduckgo.com/?q=", name:"q"};
    const searchEngines = [
      {key: ':', url: "https://duckduckgo.com/?q=", name: "q" },
      {key: 'd', url: "https://duckduckgo.com/?q=", name: "q" },
      {key: 'g', url: "https://www.google.com/search?q=", name: "q" },
      {key: 'mdn', url: "https://developer.mozilla.org/en-US/search?q=", name: "q" },
      {key: 'mdnja', url: "https://developer.mozilla.org/ja/search?q=", name: "q" },
      {key: 'weblio',
       url: "https://ejje.weblio.jp/content_find?searchType=exact&query=",
       name: "query" },
      {key: 'dlsite',
       url: "http://www.dlsite.com/maniax/fsr/=/language/jp/sex_category[0]/male/keyword/",
       name: ""},
      {key: 'gentoo-package', name: "q", url: "https://packages.gentoo.org/packages/search?q="},
      {key: 'scryfall', name: "q", url: "https://scryfall.com/search?q="}
    ];
    const searchEngine = undefinedOptionS(defaultSearchEngine)(
      searchEngines.find((record) => record.key == mode.key)
    );
    //const searchEngineUrl = undefinedMaybeS(defaultSearchEngineUrl, (record) => record.url)(
    //searchEngines.find((record) => record.key == mode.key));
    const searchEngineUrl = searchEngine.url;

    const pad = (isDefined(searchEngine.key)) ? "" : (mode.key + "+");
    const href = searchEngineUrl + pad + mode.query.join('+');
    window.location.assign(href); // should work on _top
  }
}

function addShotBox() {
  oneShotState.shotbox =
    nullOption(() => {
      const shotbox = document.createElement("input");
      shotbox.setAttribute('style', mkShotBoxStyle(false) );
      shotbox.onblur = hideShotBox;
      shotbox.addEventListener('keydown', (e) => {
        const k = Stroke.upkey(e);
        if ( [{key: "Enter"}, {ctrl: true, key: 'm'}].some( Stroke.eqKey(k) ) ) {
          fireShotbox(shotbox.value);
          hideShotBox();
        }
      });
      document.body.appendChild(shotbox);
      return shotbox;
    })(oneShotState.shotbox);
  oneShotState.shotboxList =
    nullOption(() => {
      const list = document.createElement("ul");

      document.body.appendChild(list);
      return list
    })(oneShotState.shotboxList);
  // const shotboxList = createElement("ul");
  oneShotState.behindBox =
    nullOption(() => {
      const behindBox = document.createElement('div');
      behindBox.setAttribute('style', "z-index: -100000; display: none;");
      return behindBox;
    })(oneShotState.behindBox);
  oneShotState.extraBookmarkPopup =
    nullOption(() => {
      const state = {
        data: {title: "", name: "", url: "", suggestedTags: [], tags: []},
        control: {}
      };
      const popup = document.createElement('div');
      state.element = popup;

      state.control.setVisible = function(b=false){
        const t = ["z-index: 100000"];
        const x = (!b) ? (t.concat(["display: none"])) : t;
        state.element.setAttribute('style', x.concat([""]).join("; "));
      };
      state.control.hide = function(){state.control.setVisible(false);};

      state.control.hide();
      return state;
    })(oneShotState.extraBookmarkPopup);
}

function mkShotBoxStyle(visible = false) {
  const t =
        [ "position: fixed",
          "width: " + oneShotConfig.shotboxLayout.width,
          "height: " + oneShotConfig.shotboxLayout.height,
          "top: " + oneShotConfig.shotboxLayout.top,
          "left: " + oneShotConfig.shotboxLayout.left,
          "margin: " + oneShotConfig.shotboxLayout.margin,
          "padding: " + oneShotConfig.shotboxLayout.padding,
          "border: " + oneShotConfig.shotboxLayout.border,
          "background: " + oneShotConfig.shotboxDesign.background,
          "font-size: 14px",
          "z-index: 1000001", 
          "color: " + oneShotConfig.shotboxDesign.color ];
  const x = (!visible) ? (t.concat(["display: none"])) : t;
  return x.concat([""]).join("; ");
}

function hideShotBox() {
  nullForeach((shotbox) => {
    shotbox.setAttribute('style', mkShotBoxStyle(false));
    shotbox.value = "";
    return shotbox;
  })(oneShotState.shotbox);
}
function showShotBox(initval) {
  if (oneShotState.shotbox == null) { addShotBox(); }
  nullForeach((shotbox) => {
    shotbox.setAttribute('style', mkShotBoxStyle(true));
    shotbox.value = initval;
    const M = initval.length;
    shotbox.focus(); // element.focus() was not supported on webkit
    shotbox.setSelectionRange(M,M);
  })(oneShotState.shotbox);
}

// off focus... fired, quitted, click other elem, lost focus
// onLostFocus.addEventListener?
// onblur completely work?
// shotboxList should not keep focus


// boxitem {hidden: <bool>, text?: }
// visible <> all?
// all ? incremental?



function addIndicator() {
  oneShotState.indicator =
    nullOption(() => {
      const indicator = document.createElement("span");
      indicator.setAttribute('style', mkIndicatorStyle(false));
      document.body.appendChild(indicator);
      return indicator;
    }
    )(oneShotState.indicator);
}
function hideIndicator() {
  nullForeach((indicator) => {
    indicator.setAttribute('style', mkIndicatorStyle(false));
    indicator.innerText = "";
    dropIndicatorTimer();
    return indicator;
  })(oneShotState.indicator);
}
function dropIndicatorTimer() {
  oneShotState.indicatorTimer = nullBind(() => (timer) => {
    window.clearTimeout(timer);
    return null;
  })(oneShotState.indicatorTimer);
}
function showIndicator(str) {
  if (oneShotState.indicator == null) { addIndicator(); }
  nullForeach((indicator) => {
    indicator.setAttribute('style', mkIndicatorStyle(true));
    indicator.innerText = str;
    dropIndicatorTimer();
    oneShotState.indicatorTimer = window.setTimeout(hideIndicator, oneShotConfig.indicatorTime);
    return indicator;
  })(oneShotState.indicator);
}
// infomation showing control?

function mkIndicatorStyle(visible = false) {
  const t =
        [ "position: fixed",
          "width: " + oneShotConfig.indicatorLayout.width,
          "height: " + oneShotConfig.indicatorLayout.height,
          "bottom: " + oneShotConfig.indicatorLayout.bottom,
          "left: " + oneShotConfig.indicatorLayout.left,
          "margin: " + oneShotConfig.indicatorLayout.margin,
          "padding: " + oneShotConfig.indicatorLayout.padding,
          "border: " + oneShotConfig.indicatorLayout.border,
          "z-index: 1000000",
          "font: " + oneShotConfig.indicatorDesign.font ];
  const x = (!visible) ? (t.concat(["display: none"])) : t;
  return x.concat([""]).join("; ");
}




oneShotState.hideShotBox = hideShotBox;
oneShotState.showShotBox = showShotBox;

oneShotState.showIndicator = showIndicator;
