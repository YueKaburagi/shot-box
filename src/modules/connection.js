const shotBoxConnection = {
  internal: {},
  control: {}
};

shotBoxConnection.conn = browser.runtime.connect();
shotBoxConnection.control.postMessage = shotBoxConnection.conn.postMessage;
shotBoxConnection.control.pushCallback = (ix) => (f) => {
  shotBoxConnection.last = { ix: ix, func: f };
};
shotBoxConnection.internal.listener = (m) => {

  // catch broadcast re-configured message
  
  undefinedMaybe(
    () => console.warn("? ? "+JSON.stringify(m)),
    () => (last) => {
      if (m.ix == last.ix) {
        last.func(m.data);
        delete(shotBoxConnection.last);
      }}
  )(shotBoxConnection.last);
};

shotBoxConnection.conn.onMessage.addListener(shotBoxConnection.internal.listener);
shotBoxConnection.conn.onDisconnect.addListener((m) => {
  const e = undefinedOptionS(browser.runtime.lastError)(m.error);
  console.warn("[shotBox contents connection]closed: "+e);
});
