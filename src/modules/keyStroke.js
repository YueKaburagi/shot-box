
const Stroke = {internal: {}};

Stroke.anyPrefix = {};
Stroke.internal.isAnyPrefix = (x) => x === Stroke.anyPrefix;

Stroke.modifierKeys = [
  "Shift","Control","Alt","Meta"
];

Stroke.internal.sameKey =
  (lhs) => (rhs) => 
  ["ctrl","alt","meta","key"].every( (prop) => lhs[prop] == rhs[prop] );

Stroke.isValidKey =
  (k) => [k,k.ctl,k.alt,k.meta,k.key].every( isDefined );
Stroke.isValidStroke =
  (s) => Stroke.isAnyPrefixStroke(s) && Stroke.isValidKey(s[1]);
Stroke.isAnyPrefixStroke =
  (s) => (s.length > 0) && Stroke.internal.isAnyPrefix(s[0]);



Stroke.sameOf = function(ustroke){
  return function(rhs){
    if (rhs[0] === Stroke.internal.anyPrefix) {
      return ustroke[Math.max(0,ustroke -1)] == rhs[1];
    }
    if (ustroke.length != rhs.length) { return false; }
    let u=0;
    let t=0;
    while (ustroke[u] !== undefined && rhs[t] !== undefined) {
      if (Stroke.internal.sameKey(ustroke[u])(rhs[t])) {
        u++; t++;
      } else { return false; }
    }
    return true;
  };
};

Stroke.prefixOf = function(ustroke){
  return function(rhs){
    if (rhs[0] === Stroke.internal.anyPrefix) {
      // [*] C-g 以外のカタチのanyPrefix利用はしない。どっかにアサーション置いて
      return true;
    }
    let u=0;
    let t=0;
    while (ustroke[u] !== undefined) {
      if ( undefinedMaybeS( false, Stroke.internal.sameKey(ustroke[u]) )(rhs[t]) ) {
        u++; t++;
      } else {
        return false;
      }
    }
    return true;
  };
};



Stroke.upkey = function(e){
  const p = {
    ctrl: e.ctrlKey,
    alt: e.altKey,
    meta: e.metaKey,
    key: e.key,
    code: e.code  // `code` uses for debug only
  };
  return p;
};

Stroke.normalizeKey = function(k){
  if (!Stroke.internal.isAnyPrefix(k)) {
    if (k.ctrl === undefined) { k.ctrl = false; }
    if (k.alt === undefined) { k.alt = false; }
    if (k.meta === undefined) { k.meta = false; }
  }
  return k;
};
Stroke.eqKey = (lhs) => (rhs) => 
  Stroke.internal.sameKey(Stroke.normalizeKey(lhs))(Stroke.normalizeKey(rhs));


Stroke.show = function(k){
  if (Stroke.internal.isAnyPrefix(k)) { return "[*]"; }
  else {
    return (k.alt ? "A-" : "") +
      (k.ctrl ? "C-" : "") +
      (k.meta ? "M-" : "") +
      k.key;
  }
};


Stroke.showStroke = function(l){
  return l.reduce( (s,a) => s + " " + Stroke.show(a), "");
};
