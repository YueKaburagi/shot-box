

const commonFunctions = {strict: {}, lazy: {}, semi: {}};
commonFunctions.strict.id = function(x){ return x; };
commonFunctions.lazy.id = function(){
  return function(lx){ return lx; };};
commonFunctions.strict.not = function(x){ return (x != false); };
commonFunctions.lazy.not = function(lx){
  return function(){ return (lx() != false); };};
commonFunctions.strict.orElse = function(d){
  return function(x){ return (x === undefined) ? d : x; };};
commonFunctions.semi.orElse = function(ld){
  return function(x){ return (x === undefined) ? ld() : x; };};
commonFunctions.lazy.orElse = function(ld){
  return function(lx){
    return function(){
      const x = lx();
      return (x === undefined) ? ld() : x;  };};};
commonFunctions.strict._const = function(t){
  return function(_){ return t;  };};
commonFunctions.lazy._const = function(lt){
  return function(_){
    return function(){ return lt();  };};};
commonFunctions.strict.compose = function(f){
  return function(g){
    return function(x){ return f(g(x));  };};};
commonFunctions.lazy.compose = function(lf){
  return function(lg){
    return function(){
      return function(lx){ return lf()(lg()(lx()));  };};};};



const not = (x) => !x;
const isDefined = (x) => x !== undefined;
const notEqualInstance = (t) => (x) => t === x;


const ident =     /* @[a -> a] */
  () => (x) => x;
const _const =    /* @[x] -> @[a -> x] */
  (t) => () => (_) => t(); 
const lazy =      /* x -> @[x] */
  (x) => () => x;
const compose =   /* @[b -> c] -> @[a -> b] -> @[a -> c] */
  (lf) => (lg) => () => (x) => lf()(lg()(x));
const map =       /* [a -> b] -> @[a] -> @[b] */
  (f) => (la) => () => f(la());


// strict version
const composeS = (f) => (g) => (x) => f(g(x));
const identS = (x) => x;
const _constS = (t) => (_) => t;



const cloak = /* @[a] -> {get: () -> a}*/
      (lazyv) => {
  let r = undefined;

  const o = { internal: r }
  o.get = () => {
    if (r === undefined) { r = lazyv(); }
    assert(isDefined, r);
    return r;
  };
  return o;
}

const nullBind =
  (lazyf) => (x) => (x == null) ? x : lazyf()(x);
const nullMaybe =
  (_default, lazyf) => (nullable) =>
  (nullable == null) ? _default() : lazyf()(nullable);
const nullOption =
  (_default) => nullMaybe(_default, ident);

const nullMaybeS =
  (_default, func) => (x) => (x == null) ? _default : func(x);
const nullOptionS =
  (_default) => nullMaybeS(_default, identS);
const nullMap =
  (func) => nullMaybeS(null, func);
const nullForeach =
  (func) => nullMaybeS(undefined, (x) => _constS(undefined)(func(x)));



const undefinedBind =
  (lazyf) => (x) => (x === undefined) ? x : lazyf()(x);
const undefinedMaybe =
  (_default, lazyf) => (undefinedable) =>
  (undefinedable === undefined) ? _default() : lazyf()(undefinedable);
const undefinedOption =
  (_default) => undefinedMaybe(_default, ident);

const undefinedMaybeS =
  (_default, func) => (undefinedable) =>
  (undefinedable === undefined) ? _default : func(undefinedable);
const undefinedOptionS =
  (_default) => undefinedMaybeS(_default, identS);
const undefinedForeach =
  (func) => undefinedMaybeS(undefined, func);



const _left = (a) => {return {left: lazy(a)}; };
const _right = (b) => {return {right: lazy(b)}; };
const eitherBind =   /* @[a -> Either @x @b] -> Either @x @a -> Either @x @b */
  (lazyf) => (e) => (e.right === undefined) ? e : lazyf()(e.right());
const eitherMapLeft =
  (lazyf) => (e) => (e.right === undefined) ? _left(lazyf()(e.left)) : e;
const either =
  (lazyfl, lazyfr) => (e) => (e.right === undefined) ? lazyfl()(e.left()) : lazyfr()(e.right());
const isValidEither = (e) => (e.left !== undefined || e.right !== undefined);
