

const keymapPrioritis =
      [ "top-level",
        "edit",
        "text",
        "global",
        "bottom-level"
      ];
const compareKeymap = (a,b) => keymapPrioritis.indexOf(a) - keymapPrioritis.indexOf(b);
const compareRecord = (a,b) => compareKeymap(a.keymap, b.keymap);

// [].includes を直接渡しちゃダメ
const crttt = (s) => [ "date","datetime-local","email","month","number",
                       "password","search","tel","text","time","url","week"
                     ].includes(s);
const isEditableElement =
      (element) =>
      undefinedMaybeS(true, (x) => x != "readonly")(element.readonly) && (
        ( element.tagName == "INPUT" &&
          undefinedMaybeS(
            false,
            observe(crttt) )(element.type) 
        ) || (
          element.tagName == "TEXTAREA"
        )
      );



const bottom_level_keymap = [
//  {stroke: [{key: ' '}], fix: "do nothing", predicate: composeS(not)(isEditableElement)},  
  {stroke: [{ctrl: true, key: 'h'}], fix: "do nothing"},
  {stroke: [{ctrl: true, key: 'b'}], fix: "do nothing"},
  {stroke: [{ctrl: true, key: 't'}], fix: "do nothing"},
  {stroke: [{ctrl: true, key: 's'}], fix: "do nothing"},
  {stroke: [{ctrl: true, key: 'k'}], fix: "do nothing"}
]; bottom_level_keymap.forEach( (r) => {r.keymap = "bottom-level";} );
const global_keymap = [ 
  {stroke: [{ctrl: true, key: 'v'}], fix: "view next-page"},
  {stroke: [{ctrl: true, key: 'z'}], fix: "view prev-page"},
  {stroke: [{ctrl: true, key: 'p'}], fix: "view prev-line"},
  {stroke: [{ctrl: true, key: 'n'}], fix: "view next-line"},
  {stroke: [{ctrl: true, key: 'l'}], fix: "shotbox blank"},
  {stroke: [{ctrl: true, key: 'x'}, {key: 'f'}, {key: 'u'}, {key: 't'}, {key: 'a'}, {key: 'b'}, {key: 'a'}], fix: "futaba last50"},
  {stroke: [{ctrl: true, key: 'x'}, {key: 'k'}], fix: "tab close-current"},
  {stroke: [{ctrl: true, key: 'x'}, {key: 'u'}, {key: 'r'}, {key: 'l'}], fix: "copy current-tab-url"},
  {stroke: [{ctrl: true, key: 'x'}, {key: 'u'}, {key: 't'}, {key: 'i'}], fix: "copy current-tab-title-with-fix"},
  {stroke: [{ctrl: true, key: 'x'}, {key: 'u'}, {key: 'x'}, {key: 'r'}], fix: "test uxr"},
  {stroke: [{ctrl: true, key: 'c'}, {key: 'e'}], fix: "view beginning-of-page"},
  {stroke: [{ctrl: true, key: 'c'}, {key: 'c'}], fix: "view end-of-page"},
  {stroke: [{ctrl: true, key: 'c'}, {key: 'f'}], fix: "tab focus-next"},
  {stroke: [{ctrl: true, key: 'c'}, {key: 's'}], fix: "tab focus-prev"}
]; global_keymap.forEach( (r) => {r.keymap = "global";} );
const text_keymap = [
  {stroke: [{ctrl: true, key: 'b'}], fix: "emulate backward-char"},
  {stroke: [{ctrl: true, key: 'B'}], fix: "emulate backward-char with selection"},
  {stroke: [{ctrl: true, key: 'f'}], fix: "emulate forward-char"},
  {stroke: [{ctrl: true, key: 'F'}], fix: "emulate forward-char with selection"},
  {stroke: [{ctrl: true, key: 'a'}], fix: "emulate move-beginning-of-line"},
  {stroke: [{ctrl: true, key: 'e'}], fix: "emulate move-end-of-line"},
  {stroke: [{alt: true, key: 'w'}], fix: "emulate kill-ring-save"},
]; text_keymap.forEach( (r) => {r.keymap = "text"; } );
const edit_keymap = [
  {stroke: [{ctrl: true, key: 'h'}], fix: "emulate delete-backward-char"},
  {stroke: [{ctrl: true, key: 'd'}], fix: "emulate delete-forward-char"},
//  {stroke: [{ctrl: true, key: 'y'}], fix: "emulate yank"},  // forbidden execCommand('paste')
  {stroke: [{ctrl: true, key: 'k'}], fix: "emulate kill-line"}
]; edit_keymap.forEach( (r) => {r.keymap = "edit"; r.predicate = observe(isEditableElement)} );
const top_level_keymap = [
  {stroke: [Stroke.anyPrefix, {ctrl: true, key: 'g'}], fix: "command quit"}
]; top_level_keymap.forEach( (r) => {r.keymap = "top-level";} );


const all_keymap =
  top_level_keymap.concat(edit_keymap,text_keymap,global_keymap,bottom_level_keymap);
all_keymap.sort(compareRecord);


const keymapValidate = (keymap) => {

  // normalize keymaps
  keymap.forEach(
    (record) => {
      let page = functionsDict.find( (page) => page.fix == record.fix );
      record.func = undefinedOptionS({func: emulateNothing})(page).func;
      assert(isDefined, Stroke.normalizeKey);
      record.stroke.forEach( observe(Stroke.normalizeKey) );
      record.predicate = undefinedOptionS(_constS(true))(record.predicate);
      record.propagation = false;
    });

  // show warning
  keymap.forEach( (record) => {
    if (Stroke.isValidStroke(record.stroke)) {console.warn("invalid stroke: " + Stroke.showStroke(record.stroke)); }
    if (record.func === emulateNothing) { console.warn("unsolved index: " + record.fix); }
    if (record.predicate === undefined) { console.warn("no predicate: " + record.fix); }
    if (!keymapPrioritis.includes(record.keymap)) { console.warn("invalid keymap id: " + record.keymap);}
  });

};
