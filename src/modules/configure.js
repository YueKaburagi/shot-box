

// configurations
function mkPass(obj) {
  if (obj.fullList !== undefined) { return obj.fullList.includes; }
  if (obj.range !== undefined) { return (x) => obj.range.min <= x && x <= obj.range.max; }
}



const oneShotConfigAvailable = {
  nonDirectionalSelectionEnds: {
    fullList: ["forward","backward"]
  },
  strokeExpiresTime: {
    range: {min: 100, max: 3000}
  }
};

const oneShotConfig = {
  nonDirectionalSelectionEnds: "forward",
  strokeExpiresTime: 1200,
  shotboxLayout: {width: "580px", height: "26px",
                  anchor: "top-left", top: "460px", left: "460px",
                  margin: "2px", padding: "2px", border: "none"},
  shotboxDesign: {background: "#352637", color: "#fafdfb"},
  indicatorLayout: {width: "580px", height: "26px",
                    anchor: "bottom-left", bottom: "0", left: "0",
                    margin: "0", padding: "2px", border: "none"},
  indicatorDesign: {font: "11px"},
  indicatorTime: 2600,

  global_keymap: []
};

function configEpiLoad(config) {

  config.local_keymaps = [
    {url: "^https?://.*\\.nicovideo\\.jp/watch/.*",
     keymap: cloak(() => [
      {stroke: [{key: ' '}], fix: "do nothing",
       predicate: composeS(not)(isEditableElement),
       propagation: true, keymap: "bottom-level"}
    ])}
  ];

  config.local_keymaps.forEach( (card) => {
    card.url_pattern = new RegExp(card.url);
  });

  return config;
}

