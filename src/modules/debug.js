
const debuglevel = 2;
if (debuglevel != 0) { console.warn("ShotBox debug level: "+ debuglevel); }

function debugout(str, lv = 1) {
  if (debuglevel > lv) { console.debug(str); }
}

function stringify(x) {
  try {
    if (typeof x == 'function') {
      return ""+ x.name + "(" + x.length + ") := " + x.toString(); 
    }
    return JSON.stringify(x);
  } catch (_) {
    return ""+x;
  }
}

function spy(x, lv = 2, show = stringify) {
  if (debuglevel > lv) {
    console.debug("spy:"+(typeof x)+":> "+show(x));
  }
  return x;
}

function observe(f, lv = 3, show = stringify, showR = stringify) {
  return (x) => {
    if (debuglevel > lv) { console.debug("observe:"+f.name+":-> "+show(x)); }
    const r = f(x);
    if (debuglevel > lv) { console.debug("observe:"+f.name+":<- "+showR(r)); }
    return r;
  }
}

function assert(f, x, lv = 2, show = stringify) {
  if (debuglevel > lv && !f(x)) {
    console.warn("assertion failed: "+f.toString()+"\n about: "+show(x));
  }
}
function require(x, msg) {
  if (x === undefined) {
    console.error("detect undefined: "+msg);
  } else if (x === null) {
    console.warn("detect null: "+msg);
  }
}
