


//const mkKeymap = (url) => 
//  undefinedMaybeS(
//   all_keymap,
//    (card) => card.keymap.get().concat(all_keymap)
//  )(oneShotConfig.local_keymaps.find( (card) => card.url_pattern.test(url) ) );


// fullCandidates = generate with site url | a kind of major-mode keymap
const oneShotState = {
  stroke: [],
  strokeExpiresTimer: null,

  modeStack: [],
  shotbox: null,
  shotboxList: null,
  shotboxItem: [],

  behindBox: null,
  extraBookmarkPopup: null,

  indicator: null,
  indicatorTimer: null,

  //  config: null,
  config: oneShotConfig,
  control: {},

  bindMode: "full" // full | beginOnlyWithModifier
};

function stateInit(fullkeymap) {
  keymapValidate(fullkeymap);
  oneShotState.keymap = fullkeymap;
  oneShotState.candidates = fullkeymap;
}

oneShotState.control.killTimer = () => {
  oneShotState.strokeExpiresTimer =
    nullBind( () => (timer) => {
      clearTimeout(timer);
      return null;
    } )(oneShotState.strokeExpiresTimer);
};
oneShotState.control.activateTimer = () => {
  require(oneShotState.config, "[activateTimer] config");
  oneShotState.control.killTimer();
  oneShotState.strokeExpiresTimer = setTimeout(() => {
    oneShotState.control.resetStroke();
    oneShotState.strokeExpiresTimer = null;
  }, oneShotState.config.strokeExpiresTime);
};
oneShotState.control.resetStroke = () => {
  require(oneShotState.keymap, "[resetStroke] keymap");
  oneShotState.stroke = [];
  oneShotState.candidates = oneShotState.keymap;
};

oneShotState.control.shiftStateStandby = () => {
  oneShotState.control.killTimer();
  oneShotState.control.resetStroke();
};


